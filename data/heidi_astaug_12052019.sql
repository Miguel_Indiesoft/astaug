-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.39-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para astaug_site
CREATE DATABASE IF NOT EXISTS `astaug_site` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `astaug_site`;

-- Volcando estructura para tabla astaug_site.transparencia_documentos
CREATE TABLE IF NOT EXISTS `transparencia_documentos` (
  `⁯id_doc` int(11) NOT NULL AUTO_INCREMENT,
  `cod_fraccion` int(11) DEFAULT NULL,
  `nombre_formato` varchar(255) DEFAULT NULL,
  `nombre_archivo` varchar(255) DEFAULT NULL,
  `anio` varchar(10) DEFAULT NULL,
  `extension` char(5) DEFAULT NULL,
  `ruta` varchar(500) DEFAULT NULL,
  `tipo_archivo` enum('ARCHIVO','PLANTILLA') DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  `cod_usuario` int(11) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`⁯id_doc`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla astaug_site.transparencia_documentos: ~114 rows (aproximadamente)
/*!40000 ALTER TABLE `transparencia_documentos` DISABLE KEYS */;
INSERT INTO `transparencia_documentos` (`⁯id_doc`, `cod_fraccion`, `nombre_formato`, `nombre_archivo`, `anio`, `extension`, `ruta`, `tipo_archivo`, `activo`, `cod_usuario`, `fecha_creado`, `fecha_actualizado`) VALUES
	(1, 1, 'Constitución Política de lo Estados Unidos Mexicanos', 'constitucion-politica.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/constitucion-politica.pdf', 'ARCHIVO', 1, 1, '2019-05-11 16:10:52', NULL),
	(2, 1, 'Ley Federal del Trabajo', 'ley-federal-trabajo.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/ley-federal-trabajo.pdf', 'ARCHIVO', 1, 1, '2019-05-11 17:12:36', NULL),
	(3, 1, 'Anexo I Marco Normativo', 'LTAIPG26F1_I.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_I.xlsx', 'PLANTILLA', 1, 1, '2019-05-11 18:04:30', NULL),
	(4, 1, 'Ley de Transparencia y A Inf Pub Edo Gto', 'ley-transparencia.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/ley-transparencia.pdf', 'ARCHIVO', 1, 1, '2019-05-11 16:10:52', NULL),
	(5, 1, 'Estatutos Generales', 'estatutos-generales.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/estatutos-generales.pdf', 'ARCHIVO', 1, 1, '2019-05-11 16:10:53', NULL),
	(6, 2, 'Anexo II A Estructura', 'LTAIPG26F1_IIA_Estructura.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_IIA_Estructura.xlsx', 'PLANTILLA', 1, 1, '2019-05-11 16:10:54', NULL),
	(8, 2, 'Anexo II B Organigrama', 'LTAIPG26F2_IIB_Organigrama.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F2_IIB_Organigrama.xlsx', 'PLANTILLA', 1, 1, '2019-05-11 16:10:55', NULL),
	(9, 2, 'Organigrama', 'Organigrama.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/Organigrama.pdf', 'ARCHIVO', 1, 1, '2019-05-11 16:10:56', NULL),
	(10, 2, '1 Perfil CE Secretario General', '1-Perfil-CE-Secretario-General.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/1-Perfil-CE-Secretario-General.pdf', 'ARCHIVO', 1, 1, '2019-05-11 16:10:57', NULL),
	(11, 2, '2 Perfil CE Secretario de Trabajo', '2-Perfil-CE-Secretario-de-Trabajo.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/2-Perfil-CE-Secretario-de-Trabajo.pdf', 'ARCHIVO', 1, 1, '2019-05-11 16:10:58', NULL),
	(12, 2, '3 Perfil CE Secretario de Finanzas', '3-Perfil-CE-Secretario-de-Finanzas.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/3-Perfil-CE-Secretario-de-Finanzas.pdf', 'ARCHIVO', 1, 1, '2019-05-11 16:10:59', NULL),
	(13, 2, '4 Perfil CE Secretario de Actas y Acuerdos', '4-Perfil-CE-Secretario-de-Actas-y-Acuerdos.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/4-Perfil-CE-Secretario-de-Actas-y-Acuerdos.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(14, 2, '5 Perfil CE Secretario de Prensa y Difusión', '5-Perfil-CE-Secretario-de-Prensa-y-Difusi%C3%B3n.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/5-Perfil-CE-Secretario-de-Prensa-y-Difusi%C3%B3n.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(15, 2, '6 Perfil CE Secretario de Educación y Cultura', '6-Perfil-CE-Secretario-de-Educaci%C3%B3n-y-Cultura.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/6-Perfil-CE-Secretario-de-Educaci%C3%B3n-y-Cultura.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(18, 2, '7 Perfil CE Secretario de Organización', '7-Perfil-CE-Secretario-de-Organizaci%C3%B3n.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/7-Perfil-CE-Secretario-de-Organizaci%C3%B3n.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(19, 2, '8 Perfil CE Secretario de Acción Social', '8-Perfil-CE-Secretario-de-Acci%C3%B3n-Social.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/8-Perfil-CE-Secretario-de-Acci%C3%B3n-Social.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(20, 2, '9 Perfil CE Secretario de Acción Deportiva', '9-Perfil-CE-Secretario-de-Acci%C3%B3n-Deportiva.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/9-Perfil-CE-Secretario-de-Acci%C3%B3n-Deportiva.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(21, 2, '10 Perfil Personal Apoyo Administrativo', '10-Perfil-Personal-Apoyo-Administrativo.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/10-Perfil-Personal-Apoyo-Administrativo.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(22, 2, '11 Perfil Presidente Patronato', '11-Perfil-P-Presidente.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/11-Perfil-P-Presidente.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(23, 2, '12 Perfil Secretario Patronato', '12-Perfil-P-Secretario.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/12-Perfil-P-Secretario.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(24, 2, '13 Perfil Tesorero Patronato', '13-Perfil-P-Tesorero.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/13-Perfil-P-Tesorero.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(25, 2, '14 Perfil Miembro de la Comisión de Honor y Justicia', '14-Perfil-Comisionado.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/14-Perfil-Comisionado.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(26, 3, 'Anexo LTAIPG26F1 III Facultades', 'LTAIPG26F1_III%20Facultades.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_III%20Facultades.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(27, 3, '1 Facultades CE Secretario General', '1-Facultades-CE-Secretario-General.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/1-Facultades-CE-Secretario-General.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(28, 3, '2 Facultades CE Secretario de Trabajo', '2-Facultades-CE-Secretario-de-Trabajo.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/2-Facultades-CE-Secretario-de-Trabajo.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(29, 3, '3 Facultades CE Secretario de Finanzas', '3-Facultades-CE-Secretario-de-Finanzas.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/3-Facultades-CE-Secretario-de-Finanzas.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(30, 3, '4 Facultades CE Secretario de Actas y Acuerdos', '4-Facultades-CE-Secretario-de-Actas-y-Acuerdos.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/4-Facultades-CE-Secretario-de-Actas-y-Acuerdos.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(31, 3, '5 Facultades CE Secretario de Prensa y Difusión', '5-Facultades-CE-Secretario-de-Prensa-y-Difusi%C3%B3n.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/5-Facultades-CE-Secretario-de-Prensa-y-Difusi%C3%B3n.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(32, 3, '6 Facultades CE Secretario de Educación y Cultura', '6-Facultades-CE-Secretario-de-Educaci%C3%B3n-y-Cultura.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/6-Facultades-CE-Secretario-de-Educaci%C3%B3n-y-Cultura.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(33, 3, '7 Facultades CE Secretario de Organización', '7-Facultades-CE-Secretario-de-Organizaci%C3%B3n.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/7-Facultades-CE-Secretario-de-Organizaci%C3%B3n.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(34, 3, '8 Facultades CE Secretario de Acción Social', '8-Facultades-CE-Secretario-de-Acci%C3%B3n-Social.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/8-Facultades-CE-Secretario-de-Acci%C3%B3n-Social.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(35, 3, '9 Facultades CE Secretario de Acción Deportiva', '9-Facultades-CE-Secretario-de-Acci%C3%B3n-Deportiva.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/9-Facultades-CE-Secretario-de-Acci%C3%B3n-Deportiva.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(36, 3, '10 Facultades Patronato e Integrantes', '10-Facultades-Patronato-e-Integrantes.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/10-Facultades-Patronato-e-Integrantes.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(37, 3, '11 Facultades Comisión de Honor y Justicia', '11-Facultades-Comisi%C3%B3n-de-Honor-y-Justicia.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/11-Facultades-Comisi%C3%B3n-de-Honor-y-Justicia.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(38, 3, '12 Facultades Asamblea General Absoluta', '12-Facultades-Asamblea-General-Absoluta.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/12-Facultades-Asamblea-General-Absoluta.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(39, 3, '13 Facultades Asamblea General por Delegados', '13-Facultades-Asamblea-General-por-Delegados.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/13-Facultades-Asamblea-General-por-Delegados.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(40, 9, 'Fraccion IX Gastos de Representación', 'LTAIPG26F1_IX.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_IX.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(41, 13, 'Anexo XIII', 'FORMATO_XIII.xls', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/FORMATO_XIII.xls', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(42, 13, 'Domicilio Unidad de Transparencia Astaug', 'Domicilio-Unidad-de-Transparencia-Astaug.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/Domicilio-Unidad-de-Transparencia-Astaug.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(43, 19, 'Anexo XIX Servicios', '26F1_XIX.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/26F1_XIX.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(44, 19, 'A. Formato de Afiliación a la ASTAUG', 'A-Formato-Afiliacion-ASTAUG.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/A-Formato-Afiliacion-ASTAUG.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(45, 19, 'B. Formato de Afiliación al Fondo Mutualista', 'B-Formato-Afiliacion-Fondo-Mutualista.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/B-Formato-Afiliacion-Fondo-Mutualista.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(46, 19, '1 AFILIACIÓN A LA ASTAUG', '1_AFILIACION_A_LA_ASTAUG.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/1_AFILIACION_A_LA_ASTAUG.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(47, 19, '2 AFILIACIÓN AL FONDO MUTUALISTA', '2_AFILIACION_AL_FONDO_MUTUALISTA.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/2_AFILIACION_AL_FONDO_MUTUALISTA.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(48, 19, '3 BOLSA DE TRABAJO', '3_BOLSA_DE_TRABAJO.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/3_BOLSA_DE_TRABAJO.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(49, 19, '4 APOYOS', '4_APOYOS.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/4_APOYOS.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(50, 20, 'Anexo XX Trámites y Requisitos', 'LTAIPG26F1_XX.xlsx', NULL, 'pdf', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XX.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(51, 20, 'A. Formato Carta Petición', 'A-Formato-Carta-Peticion.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/A-Formato-Carta-Peticion.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(52, 20, 'B. Formato Solicitud Préstamo', 'B-Formato-Solicitud-Prestamo.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/B-Formato-Solicitud-Prestamo.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(53, 20, 'C. Formato Apoyo Varios', 'C-Formato-Apoyo-Varios.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/C-Formato-Apoyo-Varios.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(54, 20, '1 Prestámos', '1_PRESTAMOS.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/1_PRESTAMOS.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(55, 20, '2 Fondo Mutualista', '2_FONDO_MUTUALISTA.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/2_FONDO_MUTUALISTA.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(56, 20, '3 Ortopédico', '3_ORTOPEDICO.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/3_ORTOPEDICO.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(57, 20, '4 Óptico', '4_OPTICO.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/4_OPTICO.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(58, 20, '5 Guardería', 'sites/default/files/transparencia/archivos/5_GUARDERIA.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/5_GUARDERIA.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(59, 20, '6 Canastilla', '6_CANASTILLA.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/6_CANASTILLA.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(60, 20, '7 Matrimonio', '7_MATRIMONIO.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/7_MATRIMONIO.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(61, 20, '8 Médico', '8_MEDICO.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/8_MEDICO.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(62, 20, '9 Dental', '9_DENTAL.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/9_DENTAL.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(63, 20, '10 Becas', '10_BECAS.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/10_BECAS.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(64, 20, '11 Educación Especial', '11_EDUCACION_ESPECIAL.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/11_EDUCACION_ESPECIAL.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(65, 23, 'Anexo XXIIIA Plan Anual', 'XXIIIA_Plan_anual.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/XXIIIA_Plan_anual.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(66, 23, 'Anexo XXIII B Formato Erogación', 'XXIIIB_Erogacion.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/XXIIIB_Erogacion.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(67, 23, 'Anexo XXIIIC Utilización de Tiempos Oficiales', 'XXIIIC_Utilizacion_tiempos_oficiales.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/XXIIIC_Utilizacion_tiempos_oficiales.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(68, 23, 'Anexo XXIIID Gastos de Publicidad Oficial', 'XXIIID_Gastos_publidad_oficial.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/XXIIID_Gastos_publidad_oficial.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(69, 24, 'Anexo XXIV', 'LTAIPG26F1_XXIV.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XXIV.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(70, 27, 'Anexo XXVII Contratos y Convenios', 'LTAIPG26F1_XXVII.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XXVII.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(71, 29, 'Anexo XXIX Informes', 'LTAIPG26F1_XXIX.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XXIX.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(72, 29, 'INFORME DE RESULTADOS 2015 VERSIÓN PUBLICA PARA TRANSPARENCIA', 'INFORME%20DE%20RESULTADOS%202015%20VERSI%C3%93N%20PUBLICA%20PARA%20TRANSPARENCIA.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/INFORME%20DE%20RESULTADOS%202015%20VERSI%C3%93N%20PUBLICA%20PARA%20TRANSPARENCIA.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(73, 29, 'INFORME DE RESULTADOS 2016 VERSIÓN PUBLICA PARA TRANSPARENCIA', 'INFORME%20DE%20RESULTADOS%202016%20VERSI%C3%93N%20PUBLICA%20PARA%20TRANSPARENCIA.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/INFORME%20DE%20RESULTADOS%202016%20VERSI%C3%93N%20PUBLICA%20PARA%20TRANSPARENCIA.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(74, 29, 'INFORME DE RESULTADOS 2017 VERSIÓN PUBLICA PARA TRANSPARENCIA', 'INFORME%20DE%20RESULTADOS%202017%20VERSI%C3%93N%20PUBLICA%20PARA%20TRANSPARENCIA.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/INFORME%20DE%20RESULTADOS%202017%20VERSI%C3%93N%20PUBLICA%20PARA%20TRANSPARENCIA.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(75, 30, 'Anexo XXX Estadísticas', 'LTAIPG26F1_XXX.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XXX.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(76, 33, 'Anexo LTAIPG26F1 XXXIII Convenios con Sector Social y Privado', 'LTAIPG26F1_XXXIII.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XXXIII.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(77, 34, 'LTAIPG26F1 XXXIVA Inventarios Donados', 'LTAIPG26F1_XXXIVA_Inventarios_donados.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XXXIVA_Inventarios_donados.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(78, 34, 'LTAIPG26F2 XXXIVB Inv Bienes Muebles', 'LTAIPG26F2_XXXIVB_inv_bienes_muebles.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F2_XXXIVB_inv_bienes_muebles.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(79, 34, 'LTAIPG26F3 XXXIVC Inv Alta de Bienes Inmuebles', 'LTAIPG26F2_XXXIVB_inv_bienes_muebles.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F3_XXXIVC_inv_alta_de_bienes_inmuebles.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(80, 34, 'LTAIPG26F4 XXXIVD Inv Bajas de Bienes Inmuebles', 'LTAIPG26F4_XXXIVD_Inv_bajas_bienes_inmuebles.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F4_XXXIVD_Inv_bajas_bienes_inmuebles.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(81, 34, 'LTAIPG26F5 XXXIVE Inv Altas de Bienes Muebles', 'LTAIPG26F5_XXXIVE_Inv_altas_bienes_muebles.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F5_XXXIVE_Inv_altas_bienes_muebles.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(82, 34, 'LTAIPG26F6 XXXIVF Inv Bajas de Bienes Muebles', 'LTAIPG26F6_XXXIVF_Inv_bajas_bienes_muebles.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F6_XXXIVF_Inv_bajas_bienes_muebles.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(83, 34, 'LTAIPG26F7 XXXIVG Inv Bienes de Inmuebles', 'LTAIPG26F7_XXXIVG_Inv_bienes_inmuebles.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F7_XXXIVG_Inv_bienes_inmuebles.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(84, 39, 'Anexo A', 'LTAIPG26F1_XXXIXA.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XXXIXA.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(85, 39, 'Anexo B', 'LTAIPG26F2_XXXIXB.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F2_XXXIXB.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(86, 39, 'Anexo C', 'LTAIPG26F3_XXXIXC.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F3_XXXIXC.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(87, 39, 'Anexo D', 'LTAIPG26F4_XXXIXD.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F4_XXXIXD.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(88, 40, 'Anexo A XL', 'LTAIPG26F1_XLA.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XLA.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(89, 40, 'Anexo B XL', 'LTAIPG26F2_XLB.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F2_XLB.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(90, 40, 'Anexo B XL', 'LTAIPG26F2_XLB%20(1).xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F2_XLB%20(1).xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(91, 40, 'Anexo B XL', 'LTAIPG26F2_XLB%20(2).xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F2_XLB%20(2).xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(92, 41, 'Anexo', 'LTAIPG26F1_XLI.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XLI.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(93, 45, 'Anexo LTAIPG26F1 XLV', 'LTAIPG26F1_XLV.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XLV.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(94, 45, 'Catálogo 2019 ASTAUG', 'CATALOGO_2019_ASTAUG.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/CATALOGO_2019_ASTAUG.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(95, 45, 'Guia simple 2019 ASTAUG', 'CATALOGO_2019_ASTAUG.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/CATALOGO_2019_ASTAUG.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(96, 48, 'Anexo A XLVIII Otra Información Relevante', 'LTAIPG26F1_XLVIIIA.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XLVIIIA.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(97, 48, 'Anexo B XLVIII Otra Información Relevante', 'LTAIPG26F2_XLVIIIB.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F2_XLVIIIB.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(98, 48, 'Anexo C XLVIII Otra Información Relevante', 'LTAIPG26F3_XLVIIIC.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F3_XLVIIIC.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(99, 48, 'Toma de Nota Patronato 2018-2021', 'Toma-de-Nota-Patronato-2018-2021.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/Toma-de-Nota-Patronato-2018-2021.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(100, 48, 'Toma de Nota Comite 2018-2021', 'Toma-de-Nota-Comite-2018-2021.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/Toma-de-Nota-Comite-2018-2021.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(101, 48, 'Listado de Preguntas Frecuentes', '2019_1T_Preguntas_Frecuentes.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/2019_1T_Preguntas_Frecuentes.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(102, 49, 'Anexo XLIX Solicitudes de Acceso a Información y Respuestas', 'LTAIPG26F1_XLIX.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_XLIX.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(103, 50, 'Anexo A L Otras', 'LTAIPG26F1_L.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG26F1_L.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(104, 101, 'Anexo I Contratos y Convenios con Autoridades', 'LTAIPG36F1_I.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG36F1_I.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(105, 101, 'Contrato Colectivo', 'contrato-colectivo.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/contrato-colectivo.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(106, 101, 'CONVENIO DE COLABORACION ECOLOGIA ASTAUG MUNICIPIO', 'CONVENIO_DE_COLABORACION_ECOLOGIA_ASTAUG_MUNICIPIO.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/CONVENIO_DE_COLABORACION_ECOLOGIA_ASTAUG_MUNICIPIO.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(107, 101, 'PRIMA DE ANTIGUEDAD MAYO 1990', 'PRIMA_DE_ANTIGUEDAD_MAYO_1990.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/PRIMA_DE_ANTIGUEDAD_MAYO_1990.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(108, 102, 'Anexo A II Directorio del Comité Ejecutivo', 'LTAIPG36F1_II.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG36F1_II.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(109, 102, 'Directorio', '36-directorio.pdf', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/36-directorio.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(110, 103, 'Anexo LTAIPG36F1 III Padrón de Socios', 'LTAIPG36F1_III.xlsx', NULL, 'xlsx', 'sites/default/files/transparencia/archivos/LTAIPG36F1_III.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(111, 103, 'Padrón de Socios', 'Pad-01-2018.pdf', NULL, 'pdf', 'sites/default/files/transparencia/archivos/Pad-01-2018.pdf', 'ARCHIVO', 1, 1, '0000-00-00 00:00:00', NULL),
	(112, 104, 'Anexo A IV Recurso Público Recibido y Ejercido', 'LTAIPG36F1_IVA.xlsx', NULL, 'pdf', 'sites/default/files/transparencia/archivos/LTAIPG36F1_IVA.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(113, 104, 'Anexo B IV Recurso Público Recibido y Ejercido', 'LTAIPG36F2_IVB.xlsx', NULL, 'pdf', 'sites/default/files/transparencia/archivos/LTAIPG36F2_IVB.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL),
	(114, 104, 'Anexo C IV Recurso Público Recibido y Ejercido', 'LTAIPG36F3_IVC_20190314.xlsx', NULL, 'pdf', 'sites/default/files/transparencia/archivos/LTAIPG36F3_IVC_20190314.xlsx', 'PLANTILLA', 1, 1, '0000-00-00 00:00:00', NULL);
/*!40000 ALTER TABLE `transparencia_documentos` ENABLE KEYS */;

-- Volcando estructura para tabla astaug_site.transparencia_fracciones
CREATE TABLE IF NOT EXISTS `transparencia_fracciones` (
  `id_fraccion` int(11) NOT NULL AUTO_INCREMENT,
  `fraccion` char(10) DEFAULT NULL,
  `nomenclatura` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `cod_usuario` int(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0',
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id_fraccion`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla astaug_site.transparencia_fracciones: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `transparencia_fracciones` DISABLE KEYS */;
INSERT INTO `transparencia_fracciones` (`id_fraccion`, `fraccion`, `nomenclatura`, `descripcion`, `cod_usuario`, `activo`, `fecha_creado`, `fecha_actualizado`) VALUES
	(1, '1', 'I', 'I. MARCO NORMATIVO', 1, 1, '2019-05-11 16:07:10', '0000-00-00 00:00:00'),
	(2, '2', 'II', 'II. Estructura Orgánica', 1, 1, '2019-05-11 21:29:14', NULL),
	(3, '3', 'III', 'III. Facultades', 1, 1, '2019-05-12 00:30:57', NULL),
	(4, '9', 'IX', 'IX. Gastos de Representación', 1, 1, '2019-05-12 00:38:15', NULL),
	(5, '13', 'XIII', 'XIII. Domicilio Unidad de Transparencia', 1, 1, '2019-05-12 00:47:24', NULL),
	(6, '19', 'XIX', 'XIX. Servicios', 1, 1, '2019-05-12 00:48:51', NULL),
	(7, '20', 'XX', 'XX. Trámites y Requisitos', 1, 1, '2019-05-12 00:49:34', NULL),
	(8, '23', 'XXIII', 'XXIII. Comunicación Social y Publicidad Oficial', 1, 1, '2019-05-12 00:50:22', NULL),
	(9, '24', 'XXIV', 'XXIV. Auditorías Presupuestales', 1, 1, '2019-05-12 00:51:30', NULL),
	(10, '27', 'XXVII', 'XXVII. Contratos y Convenios', 1, 1, '2019-05-12 00:52:51', NULL),
	(11, '29', 'XXIX', 'XXIX. Informes', 1, 1, '2019-05-12 00:54:39', NULL),
	(12, '30', 'XXX', 'XXX. Estadísticas', 1, 1, '2019-05-12 01:33:44', NULL),
	(13, '33', 'XXXIII', 'XXXIII. Convenios con Sector Social y Privado', 1, 1, '2019-05-12 01:34:47', NULL),
	(14, '34', 'XXXIV', 'XXXIV. Muebles e Inmuebles', 1, 1, '2019-05-12 01:35:37', NULL),
	(15, '39', 'XXXIX', 'XXXIX. Actas y Resoluciones del Comité de Transparencia', 1, 1, '2019-05-12 01:35:32', NULL),
	(16, '40', 'XL', 'XL', 1, 1, '2019-05-12 01:36:08', NULL),
	(17, '41', 'XLI', 'XLI. Estudios', 1, 1, '2019-05-12 01:36:31', NULL),
	(18, '45', 'XLV', 'XLV. Instrumentos de Control Archivística', 1, 1, '2019-05-12 01:36:57', NULL),
	(19, '48', 'XLVIII', 'XLVIII. Otra Información Relevante', 1, 1, '2019-05-12 01:37:58', NULL),
	(20, '49', 'XLIX', 'XLIX. Solicitudes de Acceso a Información y Respuestas', 1, 1, '2019-05-12 01:38:37', NULL),
	(21, '50', 'L', 'L. Otras', 1, 1, '2019-05-12 01:39:09', NULL),
	(22, '101', 'I', 'I. Contratos y Convenios con Autoridades', 1, 1, '2019-05-12 02:16:05', NULL),
	(23, '102', 'II', 'II. Directorio del Comité Ejecutivo', 1, 1, '2019-05-12 02:16:35', NULL),
	(24, '103', 'III', 'III. Padrón de Socios', 1, 1, '2019-05-12 02:16:58', NULL),
	(25, '104', 'IV', 'IV. Recurso Público Recibido y Ejercido', 1, 1, '2019-05-12 02:17:20', NULL);
/*!40000 ALTER TABLE `transparencia_fracciones` ENABLE KEYS */;

-- Volcando estructura para tabla astaug_site.transparencia_solicitudes
CREATE TABLE IF NOT EXISTS `transparencia_solicitudes` (
  `⁯id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_formato` varchar(255) DEFAULT NULL,
  `nombre_archivo` varchar(255) DEFAULT NULL,
  `anio` varchar(10) DEFAULT NULL,
  `extension` char(5) DEFAULT NULL,
  `ruta` varchar(500) DEFAULT NULL,
  `tipo_archivo` enum('ARCHIVO') DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  `cod_usuario` int(11) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`⁯id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla astaug_site.transparencia_solicitudes: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `transparencia_solicitudes` DISABLE KEYS */;
/*!40000 ALTER TABLE `transparencia_solicitudes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
