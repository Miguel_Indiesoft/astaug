<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Model_model extends CI_Model {

  function __construct() {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->db = $this->load->database('default', TRUE);
    }
}
