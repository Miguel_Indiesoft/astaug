<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Transparencia_model extends CI_Model {

  function __construct() {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->db = $this->load->database('default', TRUE);
    }

    function obtenerSolicitudes(){
				$where = "activo = 1";
				$this->db->select('*');
				if($where != NULL){
					$this->db->where($where,NULL,FALSE);
					$this->db->order_by('fecha_creado', 'asc');
				}
				$query = $this->db->get('transparencia_solicitudes');
				return $query->result();
		}

		function obtenerPlantillasFraccion($id){
				$where = "cod_fraccion= ".$id." AND activo = 1 AND tipo_archivo = 'PLANTILLA'";
				$this->db->select('*');
				if($where != NULL){
					$this->db->where($where,NULL,FALSE);
					$this->db->order_by('anio', 'desc');
				}
				$query = $this->db->get('transparencia_documentos');

				return $query->result();
		}

		function obtenerFormatoFraccion($cod, $id){
				$where = "cod_fraccion = '".$cod."'";
				$where = "⁯id_doc = '".$id."'";
				$this->db->select('*');
				//$this->db->limit(1);
				if($where != NULL){
					$this->db->where($where,NULL,FALSE);
					//$this->db->order_by('id', 'asc');
				}
				//echo $this->db->last_query();
				$query = $this->db->get('transparencia_documentos');
				return $query->row();
		}


		function obtenerFormato($id){
				$where = "cod_fraccion =".$id." AND activo = 1 AND tipo_archivo = 'ARCHIVO'";
				$this->db->select('*');
				$this->db->limit(1);
				if($where != NULL){
					$this->db->where($where,NULL,FALSE);
					//$this->db->order_by('id', 'asc');
				}
				$query = $this->db->get('transparencia_documentos');
				return $query->row();
		}

		function obtenerFraccion($id){
				$where = "fraccion=".$id."";
				$this->db->select('*');
				if($where != NULL){
					$this->db->where($where,NULL,FALSE);
					//$this->db->order_by('id', 'asc');
				}
				$query = $this->db->get('transparencia_fracciones');

				return $query->row();
		}

		function obtenerFormatosFraccion($id){
				$where = "cod_fraccion=".$id." AND activo = 1 AND tipo_archivo = 'ARCHIVO'";
				$this->db->select('*');
				if($where != NULL){
					$this->db->where($where,NULL,FALSE);
					$this->db->order_by('anio', 'desc');
				}
				$query = $this->db->get('transparencia_documentos');

				return $query->result();
		}
}
