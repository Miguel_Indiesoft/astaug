<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tramites extends CI_Controller {

	 public function __construct()
	 {
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
		//$this->load->model('Blog_model', 'blog_m');

	 }

	private $defaultData = array(
		'title' => '',
		'layout' => 'layout/lytdefault',
		'contentView' => 'vUndefined',
		'stylecss' => '',
	);

	private function _renderView($data = array())
	{
		$data = array_merge($this->defaultData, $data);
		$this->load->view($data['layout'], $data);
	}

	public function index()
	{
		$data = array();
		$data['contentView'] = 'tramites/index';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
}
