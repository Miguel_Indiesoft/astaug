<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transparencia extends CI_Controller {

	 public function __construct()
	 {
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
		$this->load->model('Transparencia_model', 'transparencia_m');

	 }

	private $defaultData = array(
		'title' => '',
		'layout' => 'layout/lytdefault',
		'contentView' => 'vUndefined',
		'stylecss' => '',
	);

	private function _renderView($data = array())
	{
		$data = array_merge($this->defaultData, $data);
		$this->load->view($data['layout'], $data);
	}

	public function solicitudes()
	 {
		 //redirect('transparencia/formatos');
		 $data = array();
		 $data['lista_solicitudes'] = $this->transparencia_m->obtenerSolicitudes();
		 $data['contentView'] = 'transparencia/solicitudes';
		 $data['scripts'] = array('astaug');
		 $this->_renderView($data);
	 }

	public function obtener_documento(){
		$data = array();
		$cod = $this->input->post('cod_fraccion');
		$id = $this->input->post('id_documento');
		$data['layout'] = 'layout/lytvacio';
		$data['formato'] = $this->transparencia_m->obtenerFormatoFraccion($cod, $id);
		$data['contentView'] = 'transparencia/visor_pdf';
		$data['success'] = '';
		$this->_renderView($data);
	}

	public function fraccion($id)
	 {
		 //redirect('transparencia/formatos');
		 $data = array();
		 $data['lista_formatos'] = $this->transparencia_m->obtenerFormatosFraccion($id);
		 $data['fraccion'] = $this->transparencia_m->obtenerFraccion($id);
		 $data['formato'] = $this->transparencia_m->obtenerFormato($id);
		 $data['lista_plantillas'] = $this->transparencia_m->obtenerPlantillasFraccion($id);
		 $data['contentView'] = 'transparencia/fraccion';
		 $data['scripts'] = array('astaug');
		 $this->_renderView($data);
	 }


	public function index()
	{
		$data = array();
		$data['contentView'] = 'transparencia/index';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
}
