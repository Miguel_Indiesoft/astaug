<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller {

	 public function __construct()
	 {
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
		//$this->load->model('Blog_model', 'blog_m');

	 }

	private $defaultData = array(
		'title' => '',
		'layout' => 'layout/lytdefault',
		'contentView' => 'vUndefined',
		'stylecss' => '',
	);

	private function _renderView($data = array())
	{
		$data = array_merge($this->defaultData, $data);
		$this->load->view($data['layout'], $data);
	}

	public function noticia()
	{
		$data = array();
		$data['contentView'] = 'noticias/noticia';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function noticia1()
	{
		$data = array();
		$data['contentView'] = 'noticias/noticia1';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function noticia2()
	{
		$data = array();
		$data['contentView'] = 'noticias/noticia2';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function noticia3()
	{
		$data = array();
		$data['contentView'] = 'noticias/noticia3';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function noticia4()
	{
		$data = array();
		$data['contentView'] = 'noticias/noticia4';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function noticia5()
	{
		$data = array();
		$data['contentView'] = 'noticias/noticia5';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function noticia6()
	{
		$data = array();
		$data['contentView'] = 'noticias/noticia6';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function index()
	{
		$data = array();
		$data['contentView'] = 'noticias/index';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
}
