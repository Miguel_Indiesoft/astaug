<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	 public function __construct()
	 {
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
		$this->load->library('Correo');
		//$this->load->model('Blog_model', 'blog_m');

	 }

	private $defaultData = array(
		'title' => '',
		'layout' => 'layout/lytdefault',
		'contentView' => 'vUndefined',
		'stylecss' => '',
	);

	private function _renderView($data = array())
	{
		$data = array_merge($this->defaultData, $data);
		$this->load->view($data['layout'], $data);
	}

	public function comite()
	{
		$data = array();
		$data['contentView'] = 'inicio/comite';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}

	public function enviar()
	{
		$nombre = $this->input->post('nombre');
		$email = $this->input->post('correo');
		$mensaje = $this->input->post('msg');

		$retorno = $this->correo->enviar_mensaje($nombre, $email, $mensaje);

		if ($retorno) {
			$respuesta = array(
				'msg' => TRUE,
				'retorno' => $retorno,
				'message' => 'Mensaje enviado correctamente, gracias por ponerse en contacto con nosotros',
			);
		}else {
			$respuesta = array(
				'msg' => FALSE,
				'retorno' => $retorno,
				'message' => 'No se puedo enviar el Mensaje, intentalo de nuevo',
			);
		}

		echo json_encode($respuesta);
	}

	public function index()
	{
		$data = array();
		$data['contentView'] = 'inicio/index';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function index2()
	{
		$data = array();
		$data['contentView'] = 'inicio/index2';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
	public function index3()
	{
		$data = array();
		$data['contentView'] = 'inicio/index3';
		$data['scripts'] = array('astaug');
		$this->_renderView($data);
	}
}
