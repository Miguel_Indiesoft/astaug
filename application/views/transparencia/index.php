<!-- Testimonial -->
<!-- <section id="transparencia">
    <div class="container text-center testimonial_area">
        <br>
    </div>
</section> -->
<!-- Testimonial end -->
<!-- <br> -->
<section class="parallax_tramites"></section>
<section>
    <div class="container text-center testimonial_area">
        <div class="col-md-12">
            <div class="section-heading">
                <h2>Transparencia</h2>
            </div>
        </div>
    </div>
    <div class="container testimonial_area">
        <div class="row">
            <div class="col-md-6">
                <h2>Obligaciones de Transparencia Comunes</h2>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Fracción</th>
                            <th>Opciones</th>
                        </tr>
                        <tr class="odd">
                            <td>I. Marco Normativo</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/1" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>II. Estructura Orgánica</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/2" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>III. Facultades</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/3" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>IX. Gastos de Representación</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/9" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>XIII. Domicilio Unidad de Transparencia</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/13" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XIX. Servicios</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/19" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>XX. Trámites y Requisitos</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/20" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XXIII. Comunicación Social y Publicidad Oficial</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/23" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>XXIV. Auditorías Presupuestales</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/24" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XXVII. Contratos y Convenios</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/27" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>XXIX. Informes</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/29" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XXX. Estadísticas</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/30" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>XXXIII. Convenios con Sector Social y Privado</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/33" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XXXIV. Muebles e Inmuebles</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/34" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>XXXIX. Actas y Resoluciones del Comité de Transparencia</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/39" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XL</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/40" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XLI. Estudios</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/41" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XLV. Instrumentos de Control Archivística</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/45" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>

                        <tr class="odd">
                            <td>XLVIII. Otra Información Relevante</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/48" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>XLIX. Solicitudes de Acceso a Información y Respuestas</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/49" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>L. Otras</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/50" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-6 ">
                <h2>Obligaciones de Transparencia Específicas</h2>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Fracción</th>
                            <th>Opciones</th>
                        </tr>
                        <tr class="odd">
                            <td>I. Contratos y Convenios con Autoridades</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/101" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>II. Directorio del Comité Ejecutivo</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/102" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="odd">
                            <td>III. Padrón de Socios</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/103" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                        <tr class="even">
                            <td>IV. Recurso Público Recibido y Ejercido</td>
                            <td><a href="<?php echo base_url(); ?>index.php/transparencia/fraccion/104" rel="nofollow" class="btn btn-info">Ver Archivos</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Siéntete libre de contactárnos!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                </div>
                <button type="submit" class="btn submit_btn">Enviar</button>
            </form>
        </div>
    </div>
</section>
<!-- Contact form end -->
<script type="text/javascript">
    astaug.funciones.enviar_correo_contacto();
</script>
