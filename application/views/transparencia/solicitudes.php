<!-- Testimonial -->
<section id="transparencia">
    <div class="container text-center testimonial_area">
        <br>
    </div>
</section><!-- Testimonial end -->
<br>
<section>
    <div class="container text-center testimonial_area">
        <div class="col-md-12">
            <div class="section-heading">
                <h2>Respuestas a Solicitudes</h2>
            </div>
        </div>
    </div>
    <div class="container testimonial_area">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                  <tr>
                    <th ><h5><strong>Archivo</strong></h5></th>
                    <th ><h5><strong>Descargar</strong></h5></th>
                  </tr>
                <?php if ($lista_solicitudes): ?>
                  <?php foreach ($lista_solicitudes as $fila): ?>
                    <tr>
                      <td><?php echo $fila->nombre_formato ?></td>
                      <td>
                        <h4>
                          <a target="_blank" href="<?php echo base_url(); ?><?php echo $fila->ruta ?>">
                          <i class="fa fa-download" aria-hidden="true"></i>
                          </a>
                        </h4>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</section>
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Siéntete libre de contactárnos!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                </div>
                <button type="submit" class="btn submit_btn">Enviar</button>
            </form>
        </div>
    </div>
</section>
<!-- Contact form end -->
<script type="text/javascript">
    astaug.funciones.enviar_correo_contacto();
</script>
