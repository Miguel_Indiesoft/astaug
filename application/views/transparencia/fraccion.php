<!-- Testimonial -->
<section id="fraccion">
    <div class="container text-center testimonial_area"></div>
</section>
<!-- Testimonial end -->
<section class="section_pdf">
    <div class="container">
        <h2><?php echo $fraccion->descripcion; ?></h2>
        <input type="hidden" id="cod_fraccion" name="" value="<?php echo $fraccion->id_fraccion; ?>">
        <div class="row">
          <?php if ($lista_formatos): ?>
            <div class="col-sm-9">
                <div id="visor-pdf" class="">
                    <!-- <embed class="archivo_pdf" src="<?php echo base_url(); ?>sites/default/files/transparencia/archivos/<?php echo $formato->nombre_archivo; ?>"> -->
                      <object class="archivo_pdf" data="<?php echo base_url(); ?>sites/default/files/transparencia/archivos/<?php echo $formato->nombre_archivo; ?>">
                		   <p>Lo sentimos, tu navegador no tiene soporte para ver PDF, descarga el archivo para visualizarlo desde alguna aplicaci&oacute;n para ver archivos PDF <br>
                		    <a class="btn btn-info btn-lg" href="<?php echo base_url(); ?>sites/default/files/transparencia/archivos/<?php echo $formato->nombre_archivo; ?>"><i class="fa fa-download"></i> <?php echo $formato->nombre_formato; ?></a>
                			</p>
                		</object>
                </div>
            </div>
          <?php endif; ?>
            <div class="col-sm-3">
                <?php if ($lista_formatos): ?>
                    <div class="box_file">
                        <ul class="nav flex-column">
                            <h4>Archivos</h4>
                            <?php foreach ($lista_formatos as $fila): ?>
                            <li class="nav-item">
                                <a class="nav-link style_ul_nav btn-ver-documento" href="#" id="<?php echo $fila->⁯id_doc; ?>">
                                    <img class="img_pdf" src="<?php echo base_url(); ?>images/astaug/iconos/pdf.jpg" alt="">
                                    <?php echo $fila->nombre_formato; ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                  <?php endif; ?>
                <div class="box_file">
                    <ul class="nav flex-column">
                        <h4>Plantilla(s)</h4>
                        <?php if ($lista_plantillas): ?>
                        <?php foreach ($lista_plantillas as $fila): ?>
                        <li class="nav-item">
                            <a class="nav-link style_ul_nav" href="<?php echo base_url(); ?><?php echo $fila->ruta; ?>">
                                <img class="img_pdf" src="<?php echo base_url(); ?>images/astaug/iconos/descargaxls.jpg" alt="">
                                <?php echo $fila->nombre_formato; ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Siéntete libre de contactárnos!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                </div>
                <button type="submit" class="btn submit_btn">Enviar</button>
            </form>
        </div>
    </div>
</section>
<!-- Contact form end -->
<script type="text/javascript">
  astaug.funciones.enviar_correo_contacto();
	astaug.funciones.ver_documento();
</script>
