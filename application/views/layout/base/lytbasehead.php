<!DOCTYPE html>
<html lang="es"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>ASTAUG - Asociación Sindical de Trabajadores Administrativos de la Universidad de Guanajuato</title>
        <meta name="description" content="Asociación Sindical de Trabajadores Administrativos de la Universidad de Guanajuato">
        <meta name="author" content="indiesoft.com.mx, fabricio magaña ruiz, miguel soria rangel, jose moncada sanchez">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Custom Fonts -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>custom-font/fonts.css" />
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/font-awesome.min.css" />
        <!-- Bootsnav -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootsnav.css">
        <!-- Fancybox -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.fancybox.css?v=4.4.0" media="screen" />
        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/custom.css?v=1.0.12" />

        <link href='https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.css' rel='stylesheet' />

        <!-- Icon Favicon -->
        <!-- <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>images/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo base_url(); ?>images/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff"> -->

        <!-- JavaScript -->
        <script src="<?php echo base_url(); ?>js/jquery-1.12.1.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

        <script src="<?php echo base_url(); ?>js/main.js"></script>

				<script src="<?php echo base_url(); ?>js/lib/astaug.js"></script>

				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-40637562-1', 'auto');
              ga('send', 'pageview');
        </script>


    </head>
    <body>
<!-- Preloader -->


        <!-- <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                </div>
            </div>
        </div> -->


    <!--End off Preloader -->
    <!-- Header -->
    <header>
        <!-- Top Navbar -->
        <div class="top_nav">
            <div class="container">
                <ul class="list-inline info">
                    <li><a href="#"><span class="fa fa-phone"></span> 01 473 73 2 0065</a></li>
                    <li><a href="#"><span class="fa fa-envelope"></span> contacto@astaug.mx</a></li>
                    <li><a href="#"><span class="fa fa-clock-o"></span> Lunes - Viernes 9:00 - 16:00</a></li>
                    <!--  <li><a href="#"><span class="fa fa-clock-o"></span> Mon - Sat 9:00 - 19:00</a></li>-->
                </ul>
                <ul class="list-inline social_icon">
                    <li><a href="https://www.facebook.com/astaug" target="_blank"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="https://twitter.com/GtoJimenez" target="_blank"><span class="fa fa-twitter"></span></a></li>
                    <!--<li><a href=""><span class="fa fa-behance"></span></a></li>-->
                    <!--<li><a href=""><span class="fa fa-dribbble"></span></a></li>-->
                    <!--<li><a href=""><span class="fa fa-linkedin"></span></a></li>-->
                    <!--  <li><a href=""><span class="fa fa-youtube"></span></a></li>-->
                </ul>
            </div>
        </div><!-- Top Navbar end -->
        <nav class="navbar bootsnav">
            <div class="container">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                          <i class="fa fa-bars"></i>
                      </button>
                        <!--  <a class="navbar-brand" href=""><img class="logo" src="images/logo.png" alt=""></a>-->
                        <a class="navbar-brand" href="<?php echo site_url('inicio'); ?>"><img class="logo" id='logo' src="<?php echo base_url(); ?>images/astaug/astaug-logo-azul.png" alt=""></a>
                  </div>
                      <!-- Navigation -->
                      <div class="collapse navbar-collapse" id="navbar-menu">
                          <ul class="nav navbar-nav menu">
                              <li><a class="cool-link" href="<?php echo site_url('inicio'); ?>">Inicio</a></li>
                              <li><a class="cool-link" href="<?php echo site_url('noticias'); ?>">Noticias</a></li>
                              <li><a class="cool-link" href="<?php echo site_url('transparencia'); ?>">Transparencia</a></li>
                              <li><a class="cool-link" href="<?php echo site_url('tramites'); ?>">Tramites y Sevicios</a></li>
                              <li><a class="cool-link" href="<?php echo site_url('contacto'); ?>">Contacto</a></li>
                              <!-- <li><a class="cool-link" href="<?php echo site_url('inicio/index2'); ?>">Inicio 2</a></li> -->
                              <!-- <li><a class="cool-link" href="<?php echo site_url('inicio/index3'); ?>">Inicio 3</a></li> -->
                          </ul>
                      </div>
              </div>
          </nav><!-- Navbar end -->
    </header><!-- Header end -->
