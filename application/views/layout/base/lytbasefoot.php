<!-- Footer -->
<footer>
    <!-- Footer top -->
    <div class="container footer_top">
        <div class="row">
            <div class="col-lg-3 col-sm-12">
                <div class="footer_item">
                    <h4>Conócenos</h4>
                    <img id='logo_footer'  src="<?php echo base_url() ?>images/astaug/astaug-logo-azul.png" alt="astaug" />
                    <p>Es para mí es un honor, darte la bienvenida a este tú sitio ASTAUG, la cual te permitirá conocer de primera mano todas las actividades, la información, los servicios, en una palabra, el quehacer sindical, a través de ella estamos generando un vínculo de comunicación...
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <div class="footer_item">
                    <h4>Links de Interés</h4>
                    <ul class="list-unstyled footer_menu">
                        <li><a href="<?php echo site_url('inicio'); ?>"><span class="fa fa-play"></span> Inicio</a>
                        <li><a href="<?php echo site_url('noticias'); ?>"><span class="fa fa-play"></span> Noticias</a>
                        <li><a href="<?php echo site_url('transparencia'); ?>"><span class="fa fa-play"></span> Transparencia</a>
                        <li><a href="<?php echo site_url('tramites'); ?>"><span class="fa fa-play"></span> Tramites y Servicios</a>
                        <li><a href="<?php echo site_url('contacto'); ?>"><span class="fa fa-play"></span> Contacto</a>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <div class="footer_item">
                    <h4>Últimas Noticias</h4>
                    <ul class="list-unstyled post">
                      <li><a href="<?php echo site_url('noticias/noticia1'); ?>"><span class="date">10 <small>MAY</small></span> Dia de la Madre 2019</a></li>
                      <li><a href="<?php echo site_url('noticias/noticia'); ?>"><span class="date">30 <small>ABR</small></span> Dia del Niño 2019 </a></li>
                      <li><a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="date">18 <small>ENE</small></span> Entrega de Uniformes 2019</a></li>
                      <li><a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="date">02 <small>OCT</small></span> Apertura de Clinica Dental</a></li>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
							<div class="footer_item">
									<h4>Síguenos</h4>
									<ul class="list-inline footer_social_icon">
											<li><a href="https://www.facebook.com/astaug" target="_blank"><span class="fa fa-facebook"></span></a></li>
											<li><a href="https://twitter.com/GtoJimenez" target="_blank"><span class="fa fa-twitter"></span></a></li>
									</ul>
							</div>
            </div>
        </div>
    </div><!-- Footer top end -->
    <!-- Footer bottom -->
    <div class="footer_bottom text-center">
        <p class="wow fadeInRight">
            2019 Asociación Sindical de Trabajadores Administrativos de la Universidad de Guanajuato. Todos los derechos reservados. <br>
            Realizado por <a target="_blank" href="https://indiesoft.com.mx">IndieSoft</a>
        </p>
    </div><!-- Footer bottom end -->

</footer><!-- Footer end -->


<!-- Bootsnav js -->
<script src="<?php echo base_url(); ?>js/bootsnav.js"></script>
<!-- JS Implementing Plugins -->
<script src="<?php echo base_url(); ?>js/isotope.js"></script>
<script src="<?php echo base_url(); ?>js/isotope-active.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.fancybox.js?v=2.1.5"></script>

<script src="<?php echo base_url(); ?>js/jquery.scrollUp.min.js"></script>

<script src="<?php echo base_url(); ?>js/main.js"></script>


<script src="<?php echo base_url(); ?>js/menu_activo.js"></script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3"></script>

<script src='https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.js'></script>
<script>
mapboxgl.accessToken = 'pk.eyJ1IjoibXNvcmlhcmEiLCJhIjoiY2p3MDVsMWx3MDdteDQ1bW1idWYxcnpsNyJ9.I9zAyTFgVk1JYOl6E6Vu6A';
var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',
center:[-101.280657,21.013873],
zoom:15
});
element = document.createElement('div')
element.className='marker'
marker = new mapboxgl.Marker(element).setLngLat({
  lng:-101.280657,
  lat:21.013873
})
.addTo(map)

element = document.createElement('div')
element.className='marker'
marker = new mapboxgl.Marker(element).setLngLat({
  lng:-101.280944,
  lat:21.013174
})
.addTo(map)
</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3"></script>


<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>-->


</body>
</html>
