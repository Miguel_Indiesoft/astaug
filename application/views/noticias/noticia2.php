<section class="parallax_noticia2"></section>
<section class="noticia">
  <div class="container">
    <div class="rows ">
      <div class="col-md-12 col-sm-12 col-lg-12 row_border">

          <ul class="list-unstyled post">
               <li id="color_li"><a href=""><span class="date" id="color_date">18 <small>ENE</small></span>  Entrega de uniformes 2019</a></li>
               <p>
Compañeros y compañeras afiliados a la ASTAUG
El pasado viernes 18 de enero, se realizó la ceremonia para dar inicio a la entrega de uniformes para el personal administrativo de la Universidad de Guanajuato afiliados a la ASTAUG.
En el evento agradecimos al Sr. Rector Luis Felipe Guerrero Agripino y a su profesional equipo de trabajo el esfuerzo, compromiso y disposición que siempre han demostrado para atender nuestras necesidades y requerimientos laborales.
Muestra de ello es la inversión que desde ya hace algunos años se hace en nuestra “Alma Mater” para dotar de uniformes al personal administrativo de la Universidad de Guanajuato afiliados a la ASTAUG.
Gracias al trabajo y esfuerzo conjunto entre la Universidad de Guanajuato y la ASTAUG, en esta ocasión se otorgará ropa de trabajo a los 1,162 compañeros de trabajo (de base y contrato) que hasta la fecha del fallo de la compra conformábamos la ASTAUG: personal administrativo, bibliotecarios, personal de servicio, mantenimiento, vigilantes, veladores y operadores de vehículos.
Con esto la Universidad de Guanajuato no solo cumple con una de las obligaciones patronales para dotar del equipo de trabajo necesario a sus trabajadores para el desempeño de sus labores, sino que además contribuye a mejorar la imagen institucional de la Universidad de Guanajuato y a dignificar la función del personal administrativo.
Este es uno de muchos logros que trabajando juntos Universidad de Guanajuato y la ASTAUG hemos obtenido en beneficio de los nuestros compañeros trabajadores administrativos afiliados.
Este esfuerzo no bebe quedar aquí, por lo que es necesario redoblar esfuerzos. ¡Por ello estamos seguros que, con la voluntad de nuestras autoridades universitarias y el compromiso de la ASTAUG, continuando trabajando juntos vendrán muchos más logros!</p>

<p>¡Unidos continuemos avanzando con paso firme y seguro y consolidemos juntos un camino lleno de logros!
¡Por la Acción más que la palabra!
Comité Ejecutivo 2018-2021</p>
           </ul>
          <div class="grid">
              <div class="grid-sizer"></div>
                <div class="grid-item grid-item--width2 grid-item--height2">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_4.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_4.jpg" data-fancybox-group="gallery" title="Entrega de uniformes 2019"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_uniforme_1.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_1.jpg" data-fancybox-group="gallery" title="Entrega de uniformes 2019"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>

              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_uniforme_2.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_2.jpg" data-fancybox-group="gallery" title="Entrega de uniformes 2019"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_uniforme_3.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_3.jpg" data-fancybox-group="gallery" title="Entrega de uniformes 2019"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              </div>
            </div>

      </div>
    </div>
  </div>

</section>
<!--<section class="noticias_interes">

  <div class="container estilointeres">
    <h3>TAMBIÉN TE PUEDE INTERESAR</h3>
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-4">
        <a href="<?php echo site_url('noticias/noticia1'); ?>">
        <div  class="box_noticia_interes">
          <img  class="img-responsive" src="<?php echo base_url() ?>images/astaug/galeria/fotos/Feliz_Dia_Mama.jpg" alt="">
          <div class="interes_titulo">
            <h4>Dia de la madre 2019</h4>
          </div>
          <div class="interes_fecha">
           <a href="">10 mayo, 2019</a>
          </div>
        </div>
      </a>
      </div>
      <div class="col-sm-12 col-md-12 col-lg-4">
        <a href="<?php echo site_url('noticias/noticia5'); ?>">
        <div class="box_noticia_interes">
          <img class="img-responsive" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_3.jpeg" alt="">
          <div class="interes_titulo">
            <h4>Celebración de Cumpleaños Mensual</h4>
          </div>
          <div class="interes_fecha">
            <a href="">30 abril, 2019</a>
          </div>
        </div>
        </a>
      </div>
      <div class="col-sm-12 col-md-12 col-lg-4">
        <a href="<?php echo site_url('noticias/noticia3'); ?>">
        <div class="box_noticia_interes">
          <img class="img-responsive" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_1.jpg" alt="">
          <div class="interes_titulo">
           <h4>Apertura de Clinica Dental</h4>
          </div>
          <div class="interes_fecha">
           <a href="">01 enero, 2019</a>
          </div>
        </div>
        </a>
      </div>
    </div>
  </div>
</section>-->
<!--<section class="formulario_comentarios">
  <div class="container">
    <div class="rows">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <h3>DEJA UN COMENTARIO</h3>
        <p>Comparte con nosotros tus comentarios,sugerencias o preguntas... Todos los comentarios serán bienvenidos. </p>
        <form>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control input-lg" id="inputnombre" required>
            </div>
          </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Correo Electronico:</label>
          <div class="col-sm-10">
            <input type="email" class="form-control  input-lg" id="inputEmail" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Mensaje:</label>
          <div class="col-sm-10">
            <textarea class="form-control" id="inputmensaje" laceholder="Mensaje" rows="6" requiered></textarea>
          </div>
        </div>

    <div class="form-group">
      <div class="col-sm-12">
        <button type="submit" class="btn btn-primary btn-lg">Enviar Comentario</button>
      </div>
    </div>
  </form>
      </div>

  </div>
</section>-->
