<!-- <section id="noticias">
    <div class="container text-center testimonial_area">
      <br>
    </div>
</section>-->
<section class="parallax"></section>
<section class="noticia">
  <div class="container">
    <div class="rows ">
      <div class="col-md-12 col-sm-12 col-lg-12 row_border">

          <ul class="list-unstyled post">
               <li id="color_li"><a href=""><span class="date" id="color_date">20 <small>AUG</small></span>  Dia del Niño 2019</a></li>
               <p>Se llevó acabo la entrega del kit deportivo a un total de 620 niños menores de 10 años.</p>
           </ul>
          <div class="grid">
              <div class="grid-sizer"></div>
                <div class="grid-item grid-item--width2 grid-item--height2">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_1.jpeg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_1.jpeg" data-fancybox-group="gallery" title="Dia del Niño 2019"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_kit_2.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_2.jpeg" data-fancybox-group="gallery" title="Dia del Niño 2019"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              </div>
            </div><!-- Portfolio grid end -->

      </div>
    </div>
  </div>

</section>
