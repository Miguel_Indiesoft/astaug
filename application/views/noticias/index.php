<!--<section id="noticias">
    <div class="container text-center testimonial_area">
      <br>
    </div>
</section>-->
<!-- Testimonial end -->
<section class="parallax_noticias"></section>
<!-- last_news -->
<section id="last_news_page" class="last_news_page">
  <div class="container">
      <div  class="row sinpadding">
        <h2>Noticias</h2>
        <div class="col-md-7 col-sm-7 col-lg-7">

          <div class="media" id="box_media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="<?php echo base_url() ?>images/astaug/galeria/fotos/Banner_astaug.jpg" style="width:120px">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Día del Trabajador Universitario <small><i>Publicado 19 de Junio 2019</i></small></h4>
                <p>Un año más de ésta ya bella tradición de conmemorar el Día del trabajador Universitario,donde se promueve el Deporte y la sana convivencia...</p>
                <a class="btn btn-outline-primary" href="<?php echo site_url('noticias/noticia6'); ?>">Ver Noticia</a>
              </div>
          </div>

          <div class="media" id="box_media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="<?php echo base_url() ?>images/astaug/ultimas_noticias/Feliz_Dia_Mama.jpg" style="width:120px">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Dia de la madre 2019 <small><i>Publicado 10 de Mayo 2019</i></small></h4>
                <p>Se entrego un bono de $1000 y un detalle que consta de una cosmetiquera a un total de 460 trabajadoras ...</p>
                <a class="btn btn-outline-primary" href="<?php echo site_url('noticias/noticia1'); ?>">Ver Noticia</a>
              </div>
          </div>

          <div class="media" id="box_media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="<?php echo base_url() ?>images/astaug/ultimas_noticias/img_uniforme.jpeg" style="width:120px">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Dia del Niño 2019 <small><i>Publicado 30 de Abril 2019</i></small></h4>
                <p>Se llevó acabo la entrega del kit deportivo a un total de 620 niños menores de 10 años...</p>
              <a class="btn btn-outline-primary" href="<?php echo site_url('noticias/noticia'); ?>">Ver Noticia</a>
              </div>
          </div>

          <div class="media" id="box_media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="<?php echo base_url() ?>images/astaug/ultimas_noticias/entrega_uniformes.jpg" style="width:120px">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Entrega de uniformes 2019 <small><i>Publicado 18 de Enero 2019</i></small></h4>
                <p>El pasado viernes 18 de enero, se realizó la ceremonia para dar inicio a la entrega de uniformes para el personal administrativo ...</p>
                <a class="btn btn-outline-primary" href="<?php echo site_url('noticias/noticia2'); ?>">Ver Noticia</a>
              </div>
          </div>

          <div class="media" id="box_media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="<?php echo base_url() ?>images/astaug/ultimas_noticias/clinicadental.jpg" style="width:100px">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Apertura de Clinica Dental <small><i>Publicado 02 de Octubre 2019</i></small></h4>
                <p>Les comunicamos que ya se encuentra en funciónes las nuevas instalaciones de la Clínica Dental ASTAUG...</p>
                <a class="btn btn-outline-primary" href="<?php echo site_url('noticias/noticia3'); ?>">Ver Noticia</a>
              </div>
          </div>

          <div class="media" id="box_media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="<?php echo base_url() ?>images/astaug/ultimas_noticias/complejo_deportivo.jpg" style="width:120px">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Apertura de Complejo Deportivo <small><i>Publicado 02 de Octubre 2019</i></small></h4>
                <p>Inauguración Complejo Deportivo y colocación de la primera piedra de las nuevas oficinas de la ASTAUG...</p>
                <a class="btn btn-outline-primary" href="<?php echo site_url('noticias/noticia4'); ?>">Ver Noticia</a>
              </div>
          </div>

          <div class="media" id="box_media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="<?php echo base_url() ?>images/astaug/ultimas_noticias/cumpleanos.jpeg" style="width:120px">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Celebración de Cumpleaños Mensual <small><i>Publicado 18 de Enero 2019</i></small></h4>
                <p>Con la participación de los integrantes del comite ejecutivo se han realizado los festejos de los cumpleaños..</p>
                 <a class="btn btn-outline-primary" href="<?php echo site_url('noticias/noticia5'); ?>">Ver Noticia</a>
              </div>
          </div>


        </div>
        <div class="col-md-5 col-sm-5 col-lg-5">
                <div class="menu_lateral_links">
                <h3>Links de Interés</h3>
                <ul>
                  <li><a href="<?php echo site_url('inicio'); ?>"><span class="fa fa-play"></span> Inicio</a>
                  <li><a href="<?php echo site_url('noticias'); ?>"><span class="fa fa-play"></span> Noticias</a>
                  <li><a href="<?php echo site_url('transparencia'); ?>"><span class="fa fa-play"></span> Transparencia</a>
                  <li><a href="<?php echo site_url('tramites'); ?>"><span class="fa fa-play"></span> Tramites y Servicios</a>
                  <li><a href="<?php echo site_url('contacto'); ?>"><span class="fa fa-play"></span> Contacto</a>
                </ul>
                </div>
                <div class="text-center menu_lateral_facebook">
                  <h3>Facebook</h3>
                <div class="fb-page" data-href="https://www.facebook.com/Astaug-984146588380267/" data-tabs="timeline" data-width="300" data-height="480" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Astaug-984146588380267/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Astaug-984146588380267/">Astaug</a></blockquote></div>
                </div>
      </div>
        </div>
  </div>
</section>
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Siéntete libre de contactárnos!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                </div>
                <button type="submit" class="btn submit_btn">Enviar</button>
            </form>
        </div>
    </div>
</section>
<!-- Contact form end -->
<script type="text/javascript">
  astaug.funciones.enviar_correo_contacto();
</script>
