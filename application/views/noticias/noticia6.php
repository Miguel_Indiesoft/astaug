<section class="parallax_noticia6"></section>
<section class="noticia">
  <div class="container">
    <div class="rows ">
      <div class="col-md-12 col-sm-12 col-lg-12 row_border">

          <ul class="list-unstyled post">
               <li id="color_li"><a href=""><span class="date" id="color_date">15 <small>JUN</small></span>  Día del Trabajador Universitario</a></li>
               <p>Un agradecimiento a todos los participantes de ésta edición 2019, cada año son mas nuestros compañeros que se unen a este evento y nos llena de alegría que se animen a participar y a activarse físicamente. Felicidades a todos los ganadores de este año y esperemos vernos en la próxima edición de éste ya tradicional evento.
                 <br>"Por la acción más que la palabra"
                 <br>Comité ejecutivo 2018-2021</p>
           </ul>
          <div class="grid">
              <div class="grid-sizer"></div>
                <div class="grid-item grid-item--width2 grid-item--height2">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_PORTADA_IMG_0037.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0037.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0022.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0022.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>

              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0023.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0023.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0040.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0040.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0041.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0042.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0042.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0042.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0046.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0047.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0049.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0049.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0050.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0050.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0051.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0053.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0055 copia.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0055 copia.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0058.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0058.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0059.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0059.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0061.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0061.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0062 copia.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0062 copia.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0066.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0066.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0067.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0067.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0068.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0068.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0069.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0069.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0070.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0070.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0071.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0071.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0073.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0073.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0075.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0075.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0076.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0076.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0077.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0077.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0078.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0078.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0080.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0080.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0081.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0081.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0082.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0082.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0085.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0085.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0086.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0086.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0087.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0087.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0094.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0094.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0096.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0096.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0101.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0101.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0102 copia.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0102 copia.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0103.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0103.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0105.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0105.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0107.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0107.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0109.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0109.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0111.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0111.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0112.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0112.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0113.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0113.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0114.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0114.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0083.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0083.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0116.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0116.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0117.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0117.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0118.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0118.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0120.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0120.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0121.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0121.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0122.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0122.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0123.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0123.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0126.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0126.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0127.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0127.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0130.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0130.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0131.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0131.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0132.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0132.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0133.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0133.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0134.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0134.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0135.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0135.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0137.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0137.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0138.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0138.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0143.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0143.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0145.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0145.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0149.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0149.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0151.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0151.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0153.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0153.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0159.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0159.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0160.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0160.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0164.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0164.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0166.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0166.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>
              <div class="grid-item dia_niño_kit">
                  <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0185.jpg" >
                  <div class="portfolio_hover_area">
                      <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/galeria_trabajador_univer_2019/IMG_0185.jpg" data-fancybox-group="gallery" title="Día del Trabajador Universitario"><span class="fa fa-search"></span></a>
                      <a href="#"></a>
                  </div>
              </div>

              </div>
            </div><!-- Portfolio grid end -->

      </div>
    </div>
  </div>

</section>
<!--<section class="noticias_interes">

  <div class="container estilointeres">
    <h3>TAMBIÉN TE PUEDE INTERESAR</h3>
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-4">
        <a href="<?php echo site_url('noticias/noticia1'); ?>">
        <div  class="box_noticia_interes">
          <img  class="img-responsive" src="<?php echo base_url() ?>images/astaug/galeria/fotos/Feliz_Dia_Mama.jpg" alt="">
          <div class="interes_titulo">
            <h4>Dia de la madre 2019</h4>
          </div>
          <div class="interes_fecha">
           <a href="">10 mayo, 2019</a>
          </div>
        </div>
      </a>
      </div>
      <div class="col-sm-12 col-md-12 col-lg-4">
        <a href="<?php echo site_url('noticias/noticia5'); ?>">
        <div class="box_noticia_interes">
          <img class="img-responsive" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_3.jpeg" alt="">
          <div class="interes_titulo">
            <h4>Día del Trabajador Universitario</h4>
          </div>
          <div class="interes_fecha">
            <a href="">30 abril, 2019</a>
          </div>
        </div>
        </a>
      </div>
      <div class="col-sm-12 col-md-12 col-lg-4">
        <a href="<?php echo site_url('noticias/noticia3'); ?>">
        <div class="box_noticia_interes">
          <img class="img-responsive" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_1.jpg" alt="">
          <div class="interes_titulo">
           <h4>Apertura de Clinica Dental</h4>
          </div>
          <div class="interes_fecha">
           <a href="">01 enero, 2019</a>
          </div>
        </div>
        </a>
      </div>
    </div>
  </div>
</section>-->
<!--<section class="formulario_comentarios">
  <div class="container">
    <div class="rows">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <h3>DEJA UN COMENTARIO</h3>
        <p>Comparte con nosotros tus comentarios,sugerencias o preguntas... Todos los comentarios serán bienvenidos. </p>
        <form>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control input-lg" id="inputnombre" required>
            </div>
          </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Correo Electronico:</label>
          <div class="col-sm-10">
            <input type="email" class="form-control  input-lg" id="inputEmail" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Mensaje:</label>
          <div class="col-sm-10">
            <textarea class="form-control" id="inputmensaje" laceholder="Mensaje" rows="6" requiered></textarea>
          </div>
        </div>

    <div class="form-group">
      <div class="col-sm-12">
        <button type="submit" class="btn btn-primary btn-lg">Enviar Comentario</button>
      </div>
    </div>
  </form>
      </div>

  </div>
</section>-->
