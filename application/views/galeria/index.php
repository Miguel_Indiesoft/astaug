<section class="parallax_galeria"></section>
<section id="galeria_astaug">
    <div class="container text-center">
        <h2>GALERIA ASTAUG</h2>
        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>

        <!-- Portfolio grid -->
        <div class="grid">
            <div class="grid-sizer"></div>
            <div class="grid-item grid-item--width2 grid-item--height2 clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_1.jpg" >

                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_1.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_2.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_2.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_3.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_3.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_4.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_4.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_1.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_1.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_2.jpg" >

                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_2.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_3.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_3.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item dia_niño_kit">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_1.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_1.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item dia_niño_kit">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_2.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_2.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_1.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_1.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_2.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_2.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_3.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="images/astaug/galeria/fotos/cumple_3.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>


            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_4.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="images/astaug/galeria/fotos/cumple_4.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>



        </div><!-- Portfolio grid end -->
    </div>
</section><!-- Portfolio end -->
