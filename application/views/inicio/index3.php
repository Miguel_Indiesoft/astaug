<section id="home" class="home">
	<!-- Carousel -->
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <!-- Carousel-inner -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="image-responsive" src="<?php echo base_url() ?>images/astaug/banner_astaug/1.jpg" alt="Construction">
                <div class="overlay">
                    <div class="carousel-caption">
                        <h3>Bienvenidos</h3>
                        <h1>Asociación Sindical </h1>
                        <h1 class="second_heading">de Trabajadores Administrativos</h1>
                        <p>de la Universidad de Guanajuato</p>
                        <a href="<?php echo site_url('bienvenida'); ?>" class="btn know_btn">Conocer más</a>
                        <a href="<?php echo site_url('inicio/comite') ?>" class="btn know_btn">Comité Ejecutivo 2018-2021</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="image-responsive" src="<?php echo base_url() ?>images/astaug/banner_astaug/2.jpg" alt="Construction">
                <div class="overlay">
                    <div class="carousel-caption">
                        <h3>ASTAUG 2019</h3>
                        <h1>Inauguración</h1>
                        <h1 class="second_heading">Centro Dental</h1>
                        <p>Comite Ejécutivo 2018-2021</p>
                        <a href="<?php echo site_url('noticias/noticia3'); ?>" class="btn know_btn">Ver más</a>
                        <a  class="btn know_btn">Ver Actividades</a>
                    </div>
                </div>
            </div>
        </div><!-- Carousel-inner end -->
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div><!-- Carousel end-->
</section>
<section id="box_banner_indexdos" class="box_banner_noticias">
  <div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
          <div class="box_banner_dos">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">


            <div class="item active">
            <img  src="<?php echo base_url() ?>images/astaug/banner_astaug/3.jpg" alt="New York">
            </div>
            <div class="item">
            <img   src="<?php echo base_url() ?>images/astaug/banner_astaug/4.jpg" alt="New York">
            </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
            </a>
            </div>


          </div>
          <div class="text-center" id='box_btn_header'>
            <div class="box_noticias">
                   <a href="#"><span class="fa fa-newspaper-o"></span>Consulta aquí <br>Nuestras Noticias</a>
            </div>
            <div class="box_proximoseventos">
                   <a href="#"><span class="fa fa-calendar"></span>Consulta aquí <br>Calendario de Eventos</a>
            </div>
          </div>


        </div>
        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="facebook_head">
              <div class="fb-post" data-href="https://www.facebook.com/astaug/posts/661574807604047" data-width="500" data-show-text="true"><blockquote cite="https://developers.facebook.com/astaug/posts/661574807604047" class="fb-xfbml-parse-ignore"><p>RECUERDEN, ya estan a la venta en las oficinas de ASTAUG, No te pierdas este evento conmemorativo de nuestro querido  &quot;Mtro. Chucas&quot;</p>Publicado por <a href="https://www.facebook.com/astaug">Victor Astaug</a> en&nbsp;<a href="https://developers.facebook.com/astaug/posts/661574807604047">Jueves, 30 de mayo de 2019</a></blockquote></div>
           </div>
    </div>

  </div>
  </div>
</section>
<!-- About -->
<section id="about">
    <div class="container about_bg">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="about_content">
                    <h2>Mensaje de Bienvenida</h2>
                    </br>
                    <p>Es para mí es un honor, darte la bienvenida a este tú sitio ASTAUG, la cual te permitirá conocer de primera mano todas las actividades, la información, los servicios, en una palabra, el quehacer sindical, a través de ella estamos generando un vínculo de comunicación, entre Comité Ejecutivo, Delegados sindicales, personal de oficina ASTAUG, y lo más importante, a cada trabajador adscrito a esta asociación sindical.</p>
                    <br>
                    <h5>"POR LA ACCIÓN MÁS QUE LA PALABRA, PASOS FIRMES UN CAMINO DE LOGROS"</h5>
                    <a href="<?php echo site_url('bienvenida'); ?>" class="btn know_btn">Ver Mas</a>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="about_banner">
                    <img  src="<?php echo base_url() ?>images/astaug/Mensaje_Bienvenida.jpeg" alt="" />
                </div>
            </div>
        </div>
    </div>
</section><!-- About end -->
<!-- Why us -->
<section id="why_us">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="head_title">
                   <h2>DE INTERÉS PARA NUESTROS AFILIADOS</h2>
                  <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-leaf"></span>
                    <h4>Proximos Eventos</h4>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-futbol-o"></span>
                    <h4>Eventos Deportivos</h4>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-group"></span>
                    <h4>Convocatorias</h4>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-line-chart"></span>
                    <h4>Tramites y Servicios</h4>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
                </div>
            </div>
        </div>
    </div>
</section><!-- Why us end -->
<!-- Portfolio -->
<section id="portfolio">
    <div class="container portfolio_area text-center">
        <h2>GALERIAS ASTAUG</h2>
        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
        <div id="filters">
            <button class="button is-checked" data-filter="*">Mostrar Todo</button>
            <button class="button" data-filter=".clinicadental">Clinica Dental</button>
            <button class="button" data-filter=".uniformes">Entrega de Uniformes</button>
            <button class="button" data-filter=".dia_niño_kit">Dia del Niño</button>
            <button class="button" data-filter=".celebracion_cumpleaños">Celebración Cumpleaños</button>
            <button><a  href="<?php echo site_url('galeria'); ?>" class="btn_vergaleria">Ver Galeria Completa </a></button>

        </div>
        <!-- Portfolio grid -->
        <div class="grid">
            <div class="grid-sizer"></div>
            <div class="grid-item grid-item--width2 grid-item--height2 clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_1.jpg" >

                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_1.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_2.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_2.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_3.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_3.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_4.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_4.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_1.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_1.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_2.jpg" >

                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_2.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_3.jpg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_3.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item dia_niño_kit">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_1.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_1.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item dia_niño_kit">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_2.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_2.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_1.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_1.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_2.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_2.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_3.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="images/astaug/galeria/fotos/cumple_3.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>


            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_4.jpeg" >
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="images/astaug/galeria/fotos/cumple_4.jpeg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
        </div><!-- Portfolio grid end -->
    </div>
</section><!-- Portfolio end -->
<!-- last_news -->
<section id="last_news" class="last_news">
    <div class="container container_last_news">
        <div class="row">
          <div class="col-12">
              <div class="section-heading">
                  <h2>NOTICIAS Y PRENSA</h2>
                  <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
              </div>

              <div class="col-md-4">
                <div class="box_last_news">
                      <img src="<?php echo base_url() ?>images/astaug/ultimas_noticias/img_uniforme.jpeg"  class="img_last_news" alt="">
                  <div class="box_content_last_new">
                      <div class="box_head_new">
                        <h4>Dia del Niño 2019</h4>
                      </div>
                      <p>Se llevó acabo la entrega del kit deportivo a un total de 620 niños menores de 10 años...</p>
                  </div>
                  <div class="box_button_footer_last_news">
                    <div class="button_ver_last_news">
                          <a href="<?php echo site_url('noticias/noticia'); ?>">Ver</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                 <div class="box_last_news">
                   <img src="<?php echo base_url() ?>images/astaug/ultimas_noticias/Feliz_Dia_Mama.jpg"  class="img_last_news" alt="">
                    <div class="box_content_last_new">
                       <div class="box_head_new">
                           <h4>Dia de la madre 2019</h4>
                       </div>
                       <p>Se entrego un bono de $1000 y un detalle que consta de una cosmetiquera a un total de 460 trabajadoras ...</p>
                    </div>
                   <div class="box_button_footer_last_news">
                       <div class="button_ver_last_news">
                         <a href="<?php echo site_url('noticias/noticia1'); ?>">Ver</a>
                       </div>
                   </div>
                 </div>
              </div>

              <div class="col-md-4">
               <div class="box_last_news">
                 <img src="<?php echo base_url() ?>images/astaug/ultimas_noticias/entrega_uniformes.jpg"  class="img_last_news" alt="">
                 <div class="box_content_last_new">
                     <div class="box_head_new">
                       <h4>Entrega de uniformes 2019</h4>
                     </div>
                     <p>El pasado viernes 18 de enero, se realizó la ceremonia para dar inicio a la entrega de uniformes para el personal administrativo ...</p>
                 </div>
                 <div class="box_button_footer_last_news">
                   <div class="button_ver_last_news">
                         <a href="<?php echo site_url('noticias/noticia2'); ?>">Ver</a>
                   </div>
                 </div>
               </div>
             </div>

             <div class="col-md-4">
             <div class="box_last_news">
               <img src="<?php echo base_url() ?>images/astaug/ultimas_noticias/clinicadental.jpg" class="img_last_news" alt="">
               <div class="box_content_last_new">
                 <div class="box_head_new">
                   <h4>Apertura de Clinica Dental</h4>
                 </div>
                 <p>Les comunicamos que ya se encuentra en funciónes las nuevas instalaciones de la Clínica Dental ASTAUG...</p>
               </div>
               <div class="box_button_footer_last_news">
                 <div class="button_ver_last_news">
                   <div class="button_ver_last_news">
                         <a href="<?php echo site_url('noticias/noticia3'); ?>">Ver</a>
                   </div>
                 </div>
               </div>
            </div>
           </div>
           <div class="col-md-4">
             <div class="box_last_news">
               <img src="<?php echo base_url() ?>images/astaug/ultimas_noticias/complejo_deportivo.jpg" class="img_last_news" alt="">
               <div class="box_content_last_new">
                 <div class="box_head_new">
                   <h4>Apertura de Complejo Deportivo</h4>
                 </div>
                 <p>Inauguración Complejo Deportivo y colocación de la primera piedra de las nuevas oficinas de la ASTAUG...</p>

               </div>
               <div class="box_button_footer_last_news">
                 <div class="button_ver_last_news">
                   <div class="button_ver_last_news">
                         <a href="<?php echo site_url('noticias/noticia4'); ?>">Ver</a>
                   </div>
                 </div>
               </div>

             </div>
           </div>

           <div class="col-md-4">
             <div class="box_last_news">
                 <img src="<?php echo base_url() ?>images/astaug/ultimas_noticias/cumpleanos.jpeg" class="img_last_news" alt="">
                 <div class="box_content_last_new">
                 <div class="box_head_new">
                   <h4>Celebración de Cumpleaños Mensual</h4>
                 </div>
                 <p>Con la participación de los integrantes del comite ejecutivo se han realizado los festejos de los cumpleaños..</p>

               </div>
               <div class="box_button_footer_last_news">
                 <div class="button_ver_last_news">
                   <div class="button_ver_last_news">
                         <a href="<?php echo site_url('noticias/noticia5'); ?>">Ver</a>
                   </div>
                 </div>
               </div>

             </div>
           </div>
           <div class="col-12">
               <div class="box_button_ver_todo">
                 <a href="<?php echo site_url('noticias'); ?>" class="btn btn-info">Ver Todo</a>
               </div>
           </div>




             </div>
        </div>
    </div>
</section>
<!-- Contact form -->
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Siéntete libre de contactárnos!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                </div>
                <button type="submit" class="btn submit_btn">Enviar</button>
            </form>
        </div>
    </div>
</section>
<script type="text/javascript">
  astaug.funciones.enviar_correo_contacto();
</script>
