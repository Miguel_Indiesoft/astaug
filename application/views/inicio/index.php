<section id="home" class="home">

    <div id="carousel" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="image-responsive" src="<?php echo base_url() ?>images/astaug/banner_astaug/1.jpg" alt="Construction">
                <div class="overlay">
                    <div class="carousel-caption">
                        <h3>Bienvenidos</h3>
                        <h1>Asociación Sindical </h1>
                        <h1 class="second_heading">de Trabajadores Administrativos</h1>
                        <p>de la Universidad de Guanajuato</p>
                        <a href="<?php echo site_url('bienvenida'); ?>" class="btn know_btn">Conocer más</a>
                        <a href="<?php echo site_url('noticias') ?>" class="btn know_btn">Ver Noticias</a>

                    </div>
                </div>
            </div>
            <div class="item">
                <img class="image-responsive" src="<?php echo base_url() ?>images/astaug/banner_astaug/22.jpg" alt="Construction">
                <div class="overlay">
                    <div class="carousel-caption">
                               <h3>ASTAUG 2019</h3>
                        <h1>Día</h1>
                        <h1 class="second_heading">Día del Trabajador Universitario</h1>
                        <p>de la Universidad de Guanajuato</p>
                        <a href="<?php echo site_url('bienvenida'); ?>" class="btn know_btn">Conocer más</a>
												<a href="<?php echo site_url('noticias') ?>" class="btn know_btn">Ver Noticias</a>

                    </div>
                </div>
            </div>
            <div class="item">
                <img class="image-responsive" src="<?php echo base_url() ?>images/astaug/banner_astaug/6.jpg" alt="Construction">
                <div class="overlay">
                    <div class="carousel-caption">
                        <h3>ASTAUG 2019</h3>
                        <h1>Inauguración</h1>
                        <h1 class="second_heading">Centro Dental</h1>
                        <p>Comite Ejécutivo 2018-2021</p>
                        <a href="<?php echo site_url('noticias/noticia3'); ?>" class="btn know_btn">Ver más</a>
                        <a href="<?php echo site_url('noticias') ?>" class="btn know_btn">Ver Noticias</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="image-responsive" src="<?php echo base_url() ?>images/astaug/banner_astaug/3.jpg" alt="Construction">
                <div class="overlay">
                    <div class="carousel-caption">
                        <h3>ASTAUG 2019</h3>
                        <h1>Feliz Día </h1>
                        <h1 class="second_heading">Máma</h1>
                        <p>Ninguna idea puede expresa</p>
                        <a  class="btn know_btn">Ver más</a>
                        <a  class="btn know_btn">Ver Actividades</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="image-responsive" src="<?php echo base_url() ?>images/astaug/banner_astaug/4.jpg" alt="Construction">
                <div class="overlay">
                    <div class="carousel-caption">
                        <h3>ASTAUG 2019</h3>
                        <h1>Feliz</h1>
                        <h1 class="second_heading">Día del Niño</h1>
                        <p>Comite Ejécutivo 2018-2021</p>
                        <a href="<?php echo site_url('noticias/noticia'); ?>" class="btn know_btn">Ver más</a>
                        <a class="btn know_btn">Ver Actividades</a>
                    </div>
                </div>
            </div>
        </div>

        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<section id="box_banner_indexdos" class="box_banner_noticias">
  <div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">

          <video width="100%" controls>
          <!--<source src="<?php echo base_url() ?>images/astaug/galeria/videos/VIDEO DEL DT19.mp4"type="video/mp4">-->
          <!--<source src="<?php echo base_url() ?>images/astaug/galeria/videos/VIDEO DEL DT19.mp4"type="video/ogg">-->
          </video>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="text-center">
          <div class="fb-page tex" data-href="https://www.facebook.com/Astaug-984146588380267/" data-tabs="timeline" data-width="" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Astaug-984146588380267/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Astaug-984146588380267/">Astaug</a></blockquote></div>
        </div>
      </div>
     </div>
  </section>

<!-- About -->
<section id="about">
    <div class="container about_bg">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="about_content">
                    <h2>Mensaje de Bienvenida</h2>
                    </br>
                    <p>Es para mí es un honor, darte la bienvenida a este tú sitio ASTAUG, la cual te permitirá conocer de primera mano todas las actividades, la información, los servicios, en una palabra, el quehacer sindical, a través de ella estamos generando un vínculo de comunicación, entre Comité Ejecutivo, Delegados sindicales, personal de oficina ASTAUG, y lo más importante, a cada trabajador adscrito a esta asociación sindical.</p>
                    <br>
                    <h5>"POR LA ACCIÓN MÁS QUE LA PALABRA, PASOS FIRMES UN CAMINO DE LOGROS"</h5>
                    <a href="<?php echo site_url('bienvenida'); ?>" class="btn know_btn">Ver Mas</a>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="about_banner">
                    <img src="<?php echo base_url() ?>images/astaug/Mensaje_Bienvenida.jpeg" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Portfolio -->
<section id="portfolio">
    <div class="container portfolio_area text-center">
        <h2>GALERIA ASTAUG</h2>
        <p>
        </p>
        <div id="filters">
            <button class="button is-checked" data-filter="*">Mostrar Todo</button>
            <button class="button" data-filter=".trabajadoruni">Día del Trabajador Universitario</button>
            <button class="button" data-filter=".clinicadental">Clinica Dental</button>
            <button class="button" data-filter=".uniformes">Entrega de Uniformes</button>
            <button class="button" data-filter=".dia_niño_kit">Dia del Niño</button>
            <button class="button" data-filter=".celebracion_cumpleaños">Celebración Cumpleaños</button>
            <button><a href="<?php echo site_url('galeria'); ?>" class="btn_vergaleria">Ver Galeria Completa </a></button>

        </div>
        <!-- Portfolio grid -->
        <div class="grid">
            <div class="grid-sizer"></div>
            <div class="grid-item grid-item--width2 grid-item--height2 clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_PORTADA_IMG_0037.jpg">

                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/IMG_0037.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia6'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item trabajadoruni">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_IMG_0022.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/IMG_0022.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia6'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item trabajadoruni">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_IMG_0023.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/IMG_0023.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia6'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item trabajadoruni">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_IMG_0082.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/IMG_0082.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia6'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_dental_1.jpg">

                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_1.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_dental_2.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_2.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_dental_3.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_3.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item clinicadental">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_dental_4.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/dental_4.jpg" data-fancybox-group="gallery" title="Apertura clinica dental"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_uniforme_1.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_1.jpg" data-fancybox-group="gallery" title="ENTREGA DE UNIFORMES 2019"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_uniforme_2.jpg">

                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_2.jpg" data-fancybox-group="gallery" title="ENTREGA DE UNIFORMES 2019"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item uniformes">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_uniforme_3.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/uniforme_3.jpg" data-fancybox-group="gallery" title="ENTREGA DE UNIFORMES 2019"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item dia_niño_kit">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_kit_1.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_1.jpeg" data-fancybox-group="gallery" title="DIA DEL NIÑO 2019"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item dia_niño_kit">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_kit_2.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/kit_2.jpeg" data-fancybox-group="gallery" title="DIA DEL NIÑO 2019"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_cumple_1.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_1.jpeg" data-fancybox-group="gallery" title="CELEBRACIÓN DE CUMPLEAÑOS MENSUAL"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_cumple_2.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="<?php echo base_url() ?>images/astaug/galeria/fotos/cumple_2.jpeg" data-fancybox-group="gallery" title="CELEBRACIÓN DE CUMPLEAÑOS MENSUAL"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>

            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_cumple_3.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="images/astaug/galeria/fotos/cumple_3.jpeg" data-fancybox-group="gallery" title="CELEBRACIÓN DE CUMPLEAÑOS MENSUAL"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>


            <div class="grid-item celebracion_cumpleaños">
                <img alt="" src="<?php echo base_url() ?>images/astaug/galeria/fotos/miniaturas/MIN_cumple_4.jpg">
                <div class="portfolio_hover_area">
                    <a class="fancybox" href="images/astaug/galeria/fotos/cumple_4.jpeg" data-fancybox-group="gallery" title="CELEBRACIÓN DE CUMPLEAÑOS MENSUAL"><span class="fa fa-search"></span></a>
                    <a href="<?php echo site_url('noticias/noticia5'); ?>"><span class="fa fa-link"></span></a>
                </div>
            </div>
        </div><!-- Portfolio grid end -->
    </div>
</section><!-- Portfolio end -->
<!-- Services -->
<section id="services">
    <div class="container">
        <h2>NOTICIAS Y PRENSA</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="service_item">
                    <img src="<?php echo base_url() ?>images/astaug/galeria/fotos/ultimas_noticias/MIN_IMG_0037.jpg" width="400" height="300" alt="Our Services" class="img-rounded" />
                    <h3>Día del Trabajador Universitario</h3>
                    <p>Un año más de ésta ya bella tradición de conmemorar el Día del trabajador Universitario,donde se promueve el Deporte y la sana convivencia ....</p>
                    <a href="<?php echo site_url('noticias/noticia6'); ?>" class="btn know_btn">Ver</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_item">
                    <img src="<?php echo base_url() ?>images/astaug/galeria/fotos/ultimas_noticias/MIN_nino.jpg" width="400" height="300" alt="Our Services" class="img-rounded" />
                    <h3>Día del Niño 2019</h3>
                    <p>Se llevó acabo la entrega del kit deportivo a un total de 620 niños menores de 10 años...</p>
                    <a href="<?php echo site_url('noticias/noticia'); ?>" class="btn know_btn">Ver</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_item">
                    <img src="<?php echo base_url() ?>images/astaug/galeria/fotos/ultimas_noticias/MIN_Feliz_Dia_Mama.jpg" width="400" height="300" alt="Our Services" class="img-rounded" />
                    <h3>Día de la Madre 2019</h3>
                    <p>
                      Se entrego un bono de $1000 y un detalle que consta de una cosmetiquera a un total de 460 trabajadoras ...
                    </p>
                    <a href="<?php echo site_url('noticias/noticia1'); ?>" class="btn know_btn">Ver</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="service_item">
                    <img src="<?php echo base_url() ?>images/astaug/galeria/fotos/ultimas_noticias/MIN_entrega_uniformes.jpg" width="400" height="300" alt="Our Services" class="img-rounded" />
                    <h3>Entrega de Uniformes 2019</h3>
                    <p>
                        El pasado viernes 18 de enero, se realizó la ceremonia para dar inicio a la entrega de uniformes para el personal administrativo ...
                    </p>
                    <a href="<?php echo site_url('noticias/noticia2'); ?>" class="btn know_btn">Ver</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_item">
                    <img src="<?php echo base_url() ?>images/astaug/galeria/fotos/ultimas_noticias/MIN_clinicadental.jpg" width="400" height="300" alt="Our Services" class="img-rounded" />
                    <h3>Apertura de Clínica Dental</h3>
                    <p>Les comunicamos que ya se encuentra en funciónes las nuevas instalaciones de la Clínica Dental ASTAUG...</p>
                    <a href="<?php echo site_url('noticias/noticia3'); ?>" class="btn know_btn">Ver</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_item">

                  <img src="<?php echo base_url() ?>images/astaug/galeria/fotos/ultimas_noticias/MIN_complejo_deportivo.jpg" width="400" height="300" alt="Our Services" class="img-rounded" />
                    <h3>Apertura de Complejo de Deportivo</h3>
                    <p>
											Inauguración Complejo Deportivo y colocación de la primera piedra de las nuevas oficinas de la ASTAUG...


                    <a href="<?php echo site_url('noticias/noticia4'); ?>" class="btn know_btn">Ver</a>
                </div>
            </div>

        </div>

    </div>
</section>
<!-- Contact form -->
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Estamos para apoyarte!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                </div>
                <button type="submit" class="btn submit_btn">Enviar</button>
            </form>
        </div>
    </div>
</section>
<script type="text/javascript">
  astaug.funciones.enviar_correo_contacto();
</script>
