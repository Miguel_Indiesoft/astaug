<!-- Testimonial -->
<section id="tramites">
    <div class="container text-center testimonial_area"></div>
</section>
<!-- Testimonial -->
<section id="testimonial">
    <div class="container text-center testimonial_area">
        <h2>COMITE EJECUTIVO 2018-2021</h2>
        <p>"POR LA ACCIÓN MÁS QUE LA PALABRA, PASOS FIRMES UN CAMINO DE LOGROS"</p>



        <div class="row">
            <div class="col-md-4">
                <div class="testimonial_item">
                    <div class="testimonial_content text-left">
                      <h4>Misión</h4>
                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="testimonial_item">
                    <div class="testimonial_content">
                          <h4>Visión</h4>
                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="testimonial_item">
                    <div class="testimonial_content">
                          <h4>Valores</h4>
                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section><!-- Testimonial end -->
