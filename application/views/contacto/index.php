<!--<section id="contacto">
    <div class="container text-center testimonial_area"></div>
</section>-->

<div class="box_mapa">
      <div id="map"></div>
    </div>
<section id="last_news" class="last_news">
   <div class="container container_last_news">
     <div class="row">
       <div class="col-md-12">
           <div class="section-heading">
               <h2>CONTACTO</h2>
           </div>
        </div>
     </div>

     <div class="row">
   		<div class="col-md-8 col-sm-8">
   			<div class="form_title">
   				<h3><strong><i class="icon-pencil"></i></strong>Ingrese los datos solicitados a continuación</h3>
   				<p>

   				</p>
   			</div>
   			<div class="step">

   				<div id="message-contact"></div>
   				<form class="form" method="post" action="#" id="contactform">
   					<div class="row">
   						<div class="col-md-6 col-sm-6">
   							<div class="form-group">
   								<label>Nombre</label>
   								<input type="text" class="form-control input-lg" id="name_contact" name="name_contact" placeholder="Ingresa tu nombre" required>
   							</div>
   						</div>
   						<div class="col-md-6 col-sm-6">
   							<div class="form-group">
   								<label>Apellidos</label>
   								<input type="text" class="form-control input-lg" id="lastname_contact" name="lastname_contact" placeholder="Ingresa tus apellidos" required>
   							</div>
   						</div>
   					</div>
   					<!-- End row -->
   					<div class="row">
   						<div class="col-md-6 col-sm-6">
   							<div class="form-group">
   								<label>Correo Electrónico</label>
   								<input type="email" id="email_contact" name="email_contact" class="form-control input-lg" placeholder="Ingresa tu correo electrónico" requiered>
   							</div>
   						</div>
   						<div class="col-md-6 col-sm-6">
   							<div class="form-group">
   								<label>Télefono</label>
   								<input type="text" id="phone_contact" name="phone_contact" class="form-control input-lg" placeholder="Ingresa tu télefono" required>
   							</div>
   						</div>
   					</div>
   					<div class="row">
   						<div class="col-md-12">
   							<div class="form-group">
   								<label>Mensaje</label>
   								<textarea rows="5" id="message_contact" name="message_contact" class="form-control input-lg" placeholder="Escribe tu mensaje" style="height:200px;" required></textarea>
   							</div>
   						</div>
   					</div>
   					<div class="row">
   						<div class="col-md-6">
   							<input type="submit" value="Enviar" class="btn btn-info btn-lg" id="submit-contact">
   						</div>
   					</div>
   				</form>
   			</div>
   		</div><!-- End col-md-8 -->

   		<div class="col-md-4 col-sm-4">
   			<div class="box_style_1">
   				<h4>COMITÉ EJECUTIVO</h4>
   				<div class="datos_ubicacion">
              <span class="fa fa-map-marker"></span><p>Calle Nardos Manzana O Lote 7, <br>Fraccionamiento del bosque <br> Guanajuato, Gto., México <br>CP. 36040
              </p>
           </div>
           <div class="datos_ubicacion">
            <span class="fa fa-phone"></span><li>01 473 73 2 0065 / 01 473 73 2 5309 </li>
            </div>
            <div class="datos_ubicacion">
            <span class="fa fa-fax"></span><li>Fax	 01 473 73 2 4454</li>
           </div>
   			</div>
        <div class="box_style_1">
          <span class="tape"></span>
          <h4>PATRONATO </h4>
          	<div class="datos_ubicacion">
              <span class="fa fa-map-marker"></span><p> Calle del Bosque Manzana 2 Lote 8, <br>Fraccionamiento los Pinos <br>Guanajuato, Gto., México <br> CP. 36040
              </p>
            </div>
          <div class="datos_ubicacion">
            <span class="fa fa-phone"></span><li>01 473 73 47071</li>
          </div>
          <div class="datos_ubicacion">
            <span class="fa fa-facebook"></span><a href="https://www.facebook.com/astaug">Facebook</a>
            </div>
        </div>
        <div class="box_style_1">
          <span class="tape"></span>
          <h4>COMISIÓN DE HONOR Y JUSTICIA</h4>
          <div class="datos_ubicacion">
                <span class="fa fa-map-marker"></span><p>Calle Nardos Manzana O Lote 7, <br>Fraccionamiento del bosque <br>Guanajuato, Gto., México <br>CP. 36040
                </p>
          </div>
          <div class="datos_ubicacion">
               <span class="fa fa-phone"></span>  <li>01 473 73 2 0065	/ 01 473 73 2 5309</li>
            </div>
          <div class="datos_ubicacion">
            <span class="fa fa-fax"></span><li>Fax	 01 473 73 2 4454</li>
          </div>
        </div>


        <div class="box_style_1">
          <h4>UNIDAD Y COMITÉ DE TRANSPARENCIA</h4>
            <div class="datos_ubicacion">
                <span class="fa fa-map-marker"></span><p>Calle Nardos Manzana O Lote 7, <br>Fraccionamiento del bosque <br>Guanajuato, Gto., México <br>  CP. 36040</p>
            </div>
            <div class="datos_ubicacion">
                 <span class="fa fa-phone"></span>  <li>01 473 73 2 0065	/ 01 473 73 2 5309</li>
              </div>
            <div class="datos_ubicacion">
              <span class="fa fa-fax"></span><li>Fax	 01 473 73 2 4454</li>
            </div>
            <div class="datos_ubicacion">
              <span class="fa fa-envelope"></span><a>astaugtransparencia@hotmail.com</a>
            </div>
        </div>

   		</div><!-- End col-md-4 -->
   	</div><!-- End row -->
   </div>
</section>
