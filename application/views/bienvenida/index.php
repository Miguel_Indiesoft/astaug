<!-- <section id="bienvenida">
    <div class="container text-center testimonial_area">
      <br>
    </div>
</section>-->
<!-- Testimonial end -->
<section class="parallax_bienvenida"></section>
<!-- last_news -->
<section id="pagina_bienvenida" class="pagina_bienvenida">
  <div class="container">
      <div  class="row sinpadding">
        <h2>Mensaje de Bienvenida</h2>
        <div class="col-md-7 col-sm-7 col-lg-7">
          <p>Es para mí es un honor, darte la bienvenida a este tú sitio ASTAUG, la cual te permitirá conocer de primera mano todas las actividades, la información, los servicios, en una palabra, el quehacer sindical, a través de ella estamos generando un vínculo de comunicación, entre Comité Ejecutivo, Delegados sindicales, personal de oficina ASTAUG, y lo más importante, a cada trabajador adscrito a esta asociación sindical.</p>
          <p>Nuestra página nos permitirá, agilizar la comunicación y con ello mejorar los servicios, la modernidad, las nuevas tecnologías, nos exige innovar, así como promover una buena comunicación, con un alto grado de responsabilidad, lo que nos permitirá fortalecer e impulsar a quienes conformamos el ASTAUG,</p>
          <p>Te invito de una manera muy atenta, a que con regularidad visites este espacio, el cual fue creado especialmente para ti, al mismo tiempo la invitación la hago extensiva a toda la comunidad universitaria. </p>
          <h4>"POR LA ACCIÓN MÁS QUE LA PALABRA, PASOS FIRMES UN CAMINO DE LOGROS"</h4>
          <p class="negritas">ING. Víctor Jiménez Ramírez</p>
          <p class="negritas">Secretario General de la ASTAUG</p>
        </div>
        <div class="col-md-5 col-sm-5 col-lg-5">
                <div class="box_img_bienvenida">
                  <img src="<?php echo base_url() ?>images/astaug/img_bienvenida.jpg" alt="" class="img-rounded">
                </div>
                <!-- <div class="menu_lateral_facebook">
                <h3>Facebook</h3>
                <div class="fb-page" data-href="https://www.facebook.com/Astaug-984146588380267/" data-tabs="timeline" data-width="" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Astaug-984146588380267/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Astaug-984146588380267/">Astaug</a></blockquote></div>-->
        </div>
      </div>
  </div>

</section>
<!-- Contact form -->
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Estamos para apoyarte!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" required>
                </div>
                          <div class="form-group">
                              <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                          </div>
                          <button type="submit" class="btn submit_btn">Enviar</button>
                      </form>
                  </div>
    </div>
</section><!-- Contact form end -->
<script type="text/javascript">
  astaug.funciones.enviar_correo_contacto();
</script>
