<!-- Modal -->
<div class="modal fade" id="myModalPrestamos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelPrestamos">Solicitud de Pr&eacute;stamos</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/1_PRESTAMOS.jpg" alt="tramites" class="img-rounded" />
        <iframe src="<?php echo base_url() ?>transparencia/archivos/1_PRESTAMOS.pdf" style="width:600px; height:500px;" frameborder="0"></iframe>
        <object class="archivo_pdf" data="<?php echo base_url() ?>transparencia/archivos/1_PRESTAMOS.pdf"></object>
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/B-Formato-Solicitud-Prestamo.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Pr&eacute;stamo</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalMutualista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelMutualista">Apoyo Fondo Mutualista</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/2_FONDO_MUTUALISTA.jpg" alt="tramites" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalBolsaTrabajo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelMutualista">Bolsa de Trabajo</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/3_BOLSA_DE_TRABAJO.jpg" alt="tramites" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalOrtopedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelOrtopedico">Apoyo Ortop&eacute;dico</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/3_ORTOPEDICO.jpg" alt="tramites" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelDental">Apoyo M&eacute;dico</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/8_MEDICO.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalDental" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelDental">Apoyo Dental</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/9_DENTAL.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="myModalOptico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelOptico">Apoyo &Oacute;ptico</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/4_OPTICO.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalGuarderia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelGuarderia">Apoyo Guarder&iacute;a</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/5_GUARDERIA.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalCanastilla" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelCanastilla">Apoyo Para Canastilla</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/6_CANASTILLA.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalMatrimonio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelMatrimonio">Apoyo Para Matrimonio</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/7_MATRIMONIO.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalBecas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelMatrimonio">Apoyo Para Becas</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/10_BECAS.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalEspecial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelEspecial">Apoyo Para Educaci&oacute;n Especial</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url() ?>transparencia/archivos/11_EDUCACION_ESPECIAL.jpg" alt="dental" class="img-rounded" />
      </div>
      <div class="modal-footer">
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/A-Formato-Carta-Peticion.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Carta Petici&oacute;n</a>
        <a target="_blank" href="<?php echo base_url(); ?>transparencia/archivos/C-Formato-Apoyo-Varios.pdf" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Formato Apoyo Varios</a>
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Testimonial -->
<!--<section id="tramites">
    <div class="container text-center testimonial_area"></div>
</section>-->
<section class="parallax_noticias"></section>
<!-- Testimonial -->
<section id="why_us">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="head_title">
                    <h2>TRMITES Y SERVICIOS</h2>
                    <p>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-bank"></span>
                  <h4>Solicitud de Pr&eacute;stamos</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalPrestamos">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-book"></span>
                  <h4>Fondo Mutualista</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalMutualista">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-gears"></span>
                  <h4>Bolsa de Trabajo</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalBolsaTrabajo">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-wheelchair"></span>
                  <h4>Apoyo Ortop&eacute;dico</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalOrtopedico">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>


        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-eye"></span>
                    <h4>Apoyo &Oacute;ptico</h4>
                    <p>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalOptico">
                        Ver Requisitos
                      </button>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-home"></span>
                    <h4>Apoyo para Guarder&iacute;a</h4>
                    <p>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalGuarderia">
                        Ver Requisitos
                      </button>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-cart-plus"></span>
                    <h4>Apoyo para Canastilla</h4>
                    <p>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalCanastilla">
                        Ver Requisitos
                      </button>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="why_us_item">
                    <span class="fa fa-heart"></span>
                    <h4>Apoyo para Matrimonio</h4>
                    <p>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalMatrimonio">
                        Ver Requisitos
                      </button>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-hospital-o"></span>
                  <h4>Apoyo M&eacute;dico</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalMedico">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-stethoscope"></span>
                  <h4>Apoyo Dental</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalDental">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-graduation-cap"></span>
                  <h4>Apoyo para Becas</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalBecas">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="why_us_item">
                  <span class="fa fa-star"></span>
                  <h4>Apoyo para Educaci&oacute;n Especial</h4>
                  <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalEspecial">
                      Ver Requisitos
                    </button>
                  </p>
              </div>
          </div>
        </div>
    </div>
</section>
<section id="contact_form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>¿Tienes alguna pregunta?</h2>
                <h5 class="second_heading">¡Estamos para apoyarte!</h5>
            </div>
            <form role="form" id="contactform" class="form-inline text-right col-md-6" action="/" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo ElectrÃ³nico" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Mensaje" required></textarea>
                </div>
                <button type="submit" class="btn submit_btn">Enviar</button>
            </form>
        </div>
    </div>
</section>
<!-- Contact form end -->
<script type="text/javascript">
    astaug.funciones.enviar_correo_contacto();
</script>
