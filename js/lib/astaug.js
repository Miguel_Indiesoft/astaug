/*  astaug.js
*/
	var astaug = window.vialactea || {};
	astaug.funciones = (function () {
			var base_url = "http://localhost:8888/astaug/"; //
			return {
					enviar_correo_contacto: function () {
							$("#contactform").submit(function (event) {
									var response = $.ajax({
											beforeSend: function () {
													swal({
															title: "Enviando!",
															text: "Por favor espere",
															icon: "info",
															button: false,
															closeOnEsc: false,
															closeOnClickOutside: false,
													})
											},
											type: "POST",
											url: base_url + 'index.php/inicio/enviar',
											data: $("#contactform").serialize()
									});

									response.done(function (data) {
											swal.close();
											var result = JSON.parse(data);

											if (result.msg) {
													swal("Enviado!", result.message, "success");
													$('#contactform').trigger("reset");
													$("#contactform").find('input:text, select, textarea').val('');
											} else {
													swal("Atención!", result.message, "warning");
													$('#contactform').trigger("reset");
													$("#contactform").find('input:text, select, textarea').val('');

											}
									});

									response.fail(function (jqxhr, textStatus, error) {
											swal.close();
											var err = jqxhr.Error + ", " + textStatus + ", " + error;
											$('#contactform').trigger("reset");
											$("#contactform").find('input:text, select, textarea').val('');
											swal("Error!", "No se pudo establecer conexion con el servidor, intentelo de nuevo!", "error")
											console.log("Request Failed: " + err);
									});

									event.preventDefault();

							});
					},
					ver_documento: function () {
							$(document).on('click', '.btn-ver-documento', function () {
									//NProgress.start();
									var cod = $("#cod_fraccion").val();
									var id = $(this).prop('id');
									var value = {
											cod_fraccion: cod,
											id_documento: id
									};
									var response = $.ajax({
											beforeSend: function () {
													swal({
															title: "Obteniendo!",
															text: "Por favor espere",
															icon: "info",
															button: false,
															closeOnEsc: false,
															closeOnClickOutside: false,
													})
											},
											type: "POST",
											url: base_url + 'index.php/transparencia/obtener_documento',
											data: value,
											dataType: "html"
									});

									response.done(function (data) {
											swal.close();
											$('#visor-pdf').html(data);
									});

									response.fail(function (jqxhr, textStatus, error) {
											swal.close();
											var err = jqxhr.Error + ", " + textStatus + ", " + error;
											swal("Error!", "No se pudo establecer conexion con el servidor, intentelo de nuevo!", "error")
											console.log("Request Failed: " + err);
									});
									//NProgress.done();
							});
					},
			}
	})();
